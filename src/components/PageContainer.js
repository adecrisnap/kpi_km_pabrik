import React from 'react';
import { Route,Switch } from 'react-router-dom';
//import PrivateRoute from './PrivateRoute';
import KPILevel1 from '../pages/kpilevel1'
import KPILevel2 from '../pages/kpilevel2'
import KPILevel3 from '../pages/kpilevel3'
import KPILevel1Detail from '../pages/kpilevel1detail'
import KPILevel2Detail from '../pages/kpilevel2detail'
import KPILevel3Detail from '../pages/kpilevel3detail'
import Konfigurasi from '../pages/konfigurasi'
import Target from '../pages/target'
import TargetDetail from '../pages/targetdetail'
import KonfigurasiDetail from '../pages/konfigurasidetail'
import EditKPIPage1 from '../pages/EditKPIPage1'
import HitungKPI from '../pages/HitungKPI'
import Login from '../Login'
import Home from '../HomeBackup'
import ErrorAccess from '../pages/ErrorAccess'
import UserManagementList from '../pages/userManagementList';
import UserManagement from '../pages/userManagement';
import RolesRolesDetail from '../pages/rolesrolesdetail';
import { useLocation } from 'react-router';
import KPIMapPlant from '../pages/kpimapplant';
import KPIMapPlantEditor from '../pages/kpimapplanteditor';
import TargetDetail2 from '../pages/targetdetail2';
import TargetUpload from '../pages/uploadtarget';

const PageContainer = props => {
      const location = useLocation()
      return (
            <Switch>
                  <Route exact path='/kpilevel1' key={location.key} component={KPILevel1} />
                  <Route exact path='/kpilevel2' key={location.key} component={KPILevel2} />
                  <Route exact path='/kpilevel3' key={location.key} component={KPILevel3} />
                  <Route exact path='/kpilevel1detail/:mode/:id' key={location.key} component={KPILevel1Detail} />
                  <Route exact path='/kpilevel2detail/:mode/:id' key={location.key} component={KPILevel2Detail} />
                  <Route exact path='/kpilevel3detail/:mode/:id' key={location.key} component={KPILevel3Detail} />
                  <Route exact path='/konfigurasi/' key={location.key} component={Konfigurasi} />
                  <Route exact path='/target/' key={location.key} component={Target} />
                  <Route exact path='/targetdetail/' key={location.key} component={TargetDetail} />
                  <Route exact path='/editscore/' key={location.key} component={EditKPIPage1} />
                  <Route exact path='/konfigurasidetail/' key={location.key} component={KonfigurasiDetail} />
                  <Route exact path='/hitungkpi/' key={location.key} component={HitungKPI}/>
                  <Route exact path='/login' key={location.key} component={Login}/>
                  <Route exact path='/home' key={location.key} component={EditKPIPage1}/>
                  <Route exact path='/erroraccess' key={location.key} component={ErrorAccess}/>
                  <Route exact path='/usermanagement/' key={location.key} component={UserManagementList}/>
                  <Route exact path='/rolesrolesdetail/:mode/:roleid' key={location.key} component={RolesRolesDetail}/>
                  <Route exact path='/rolekpi/:mode/:roleid' key={location.key} component={UserManagement}/>

                  <Route exact path='/mappingkpiplantroles/' key={location.key} component={KPIMapPlant}/>
                  <Route exact path='/kpimapplanteditor/:mode/:id' key={location.key} component={KPIMapPlantEditor} />
                  <Route exact path='/targetdetail2/' key={location.key} component={TargetDetail2} />

                  <Route exact path='/uploadtarget/' key={location.key} component={TargetUpload} />
            </Switch>
      );

}

export default PageContainer
