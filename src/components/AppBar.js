import React, { useState} from 'react';
import {
  Menu,
  Container,
  Segment,
  Icon,
  Image,
  Dropdown,
  List,
} from 'semantic-ui-react';
import ConfirmationBox from './ConfirmationBox';
import PropTypes from 'prop-types';
import { Link, useHistory,Redirect } from 'react-router-dom';
import Sidebar from "react-sidebar";
import useAuthContext from '../services/stores/useAuthContext';
import { createMedia } from "@artsy/fresnel"
import Login from '../Login'

const { MediaContextProvider, Media } = createMedia({
  breakpoints: {
    sm: 0,
    md: 768,
    lg: 1024,
    xl: 1192,
  },
})

const headerlinks = [ 
    { link : '/settings'},
    { link : '/profile'},
    { link : '/help'},
    { link : '/home'}
]

const NavBarMobile = props => {
    const history = useHistory();
    const { stateContext, dispatch } = useAuthContext()
    const [ open, setOpen ] = useState(false)
    const [ sidebarOpen, setSidebarOpen ] = useState(false)
    const [ docked, setDocked ] = useState(false)
    const { 
        children,
        sidebarmenus,
        showbottommenu,
        bottommenucaption,
        menubarcolor,
        menubarinverted,
        sidebarcolor,
        sidebarinverted,
        sidebarshadow
    } = props
    
    const handleCancel = () => {
        setOpen(false)
    }

    const handleConfirm = () => {
        setOpen(false)
        dispatch({
            type: "LOGOUT",
            payload: null
        })
    }

    const signOutClick = () => 
    {
        setOpen(true)
    }

    const profileClick = () =>
    {
        history.push(headerlinks[1].link)
    }

    const helpClick = () => 
    {
        history.push(headerlinks[2].link)
    }

    const setSidebar = () => 
    {
        setDocked(!docked)
        setSidebarOpen(!sidebarOpen)
    }
    var bottommenu =  <Menu inverted borderless fixed="bottom" className="bottom-menu" style={{ marginBottom: "2.5em" } }>
        <Menu.Item>{bottommenucaption}</Menu.Item>
    </Menu>

    var sidebarcontent = 
        <div>
            <Segment basic style={{ minHeight: '85vh', maxWidth:180,textAlign: 'left'}} inverted={sidebarinverted} color={sidebarcolor}>
                <List verticalAlign='middle' selection inverted={sidebarinverted}>
                    {(sidebarmenus).map((q) => 
                        (
                            <>
                                <List.Item>
                                    <List.Icon name={q.icon_id} size='large' verticalAlign='middle'/>
                                    <List.Content>
                                        <List.Header as={Link} to={'/' + q.link_id}>{q.menu_name}</List.Header>
                                        <List.Content>{q.description}</List.Content>
                                    </List.Content>
                                </List.Item>
                            </>
                        )
                    )}
                </List>
                {(showbottommenu===true) ? bottommenu : null}
            </Segment>
        </div>
    
    /*
    <List.Description>
        {q.description}
    </List.Description>
    */
    //menu item
        var help = <b><Dropdown.Item as={Link} to={headerlinks[2].link} icon='help' text='Help'/></b>
        var settings = <b><Dropdown.Item as={Link} to={headerlinks[0].link} icon='setting' text='Settings'/></b>
        var notifications = null;//<b><Dropdown.Item as={Link} to={headerlinks[1].link} icon='bell outline' text='Notifications'/></b>
       
    //group menu
        var group_general = 
            <Dropdown text='More'>
                <Dropdown.Menu>
                    {help}
                    {settings}
                    {notifications}
                </Dropdown.Menu>
            </Dropdown> 

    //menu container 
        var menu_container = 
            <Menu.Item as='a'>
                {group_general}
            </Menu.Item>

    return (
        <React.Fragment>
            <ConfirmationBox message='Are you sure you want to quit the application ?' open={open} onYes={handleConfirm} onNo={handleCancel}/>
            <Sidebar
                sidebar={sidebarcontent}
                open={sidebarOpen}
                docked={docked}
                shadow={sidebarshadow}
                styles={{ sidebar: { marginTop: "4.5em" } }}
            >
                <Menu borderless size='small' fixed="top" pointing color={menubarcolor} inverted={menubarinverted}>
                    <Menu.Item onClick={setSidebar}>
                        <Icon name='sidebar'/>
                    </Menu.Item>
                    <Menu.Item as={Link} to={headerlinks[3].link}>
                        <Image size="mini" src="https://react.semantic-ui.com/logo.png"/>
                    </Menu.Item>
                    <Menu.Item>
                        {'Role : '}&nbsp;&nbsp;<b>{props.roleName}</b>
                    </Menu.Item>
                    <Menu.Menu position="right">
                        {menu_container}
                        <Menu.Item>
                            <Dropdown 
                                text={props.username} 
                                icon='user outline' 
                                className='icon'
                                floating
                                labeled
                                button
                            >
                                <Dropdown.Menu>
                                    <Dropdown.Item onClick={profileClick}>Profile</Dropdown.Item>
                                    <Dropdown.Divider />
                                    <Dropdown.Item onClick={helpClick}>Help</Dropdown.Item>
                                    <Dropdown.Item onClick={signOutClick}>Sign Out</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </Menu.Item>
                    </Menu.Menu>
                </Menu>
                <Container style={{marginTop: "5em"}}>{children}</Container>
            </Sidebar>
        </React.Fragment>
    );
}

const NavBarDesktop = props => {
    const history = useHistory();
    const { stateContext, dispatch } = useAuthContext()
    const [ open, setOpen ] = useState(false)
    const { 
        children,
        sidebarmenus,
        showbottommenu,
        bottommenucaption,
        menubarcolor,
        menubarinverted,
        sidebarcolor,
        sidebarinverted,
        sidebarshadow
    } = props
    const [ sidebarOpen, setSidebarOpen ] = useState(true)
    const [ docked, setDocked ] = useState(true)
   
    const handleCancel = () => {
        setOpen(false)
    }

    const handleConfirm = () => {
        setOpen(false)
        dispatch({
            type: "LOGOUT",
            payload: null
        })
    }

    const signOutClick = () => 
    {
        setOpen(true)
    }

    const profileClick = () =>
    {
        history.push(headerlinks[1].link)
    }

    const helpClick = () => 
    {
        history.push(headerlinks[2].link)
    }

    const setSidebar = () => 
    {
        setDocked(!docked)
        setSidebarOpen(!sidebarOpen)
    }

    var bottommenu =  <Menu inverted borderless fixed="bottom" className="bottom-menu" style={{ marginBottom: "2.5em" } }>
        <Menu.Item>{bottommenucaption}</Menu.Item>
    </Menu>
    var sidebarcontent = 
    <div>
        <Segment basic style={{ minHeight: '89vh', maxWidth:180, textAlign: 'left'}} inverted={sidebarinverted} color={sidebarcolor}>
            <List verticalAlign='middle' selection inverted={sidebarinverted}>
                {(sidebarmenus).map((q) => 
                    (
                       <>
                            <List.Item>
                                <List.Icon name={q.icon_id} size='large' verticalAlign='middle'/>
                                <List.Content>
                                    <List.Header as={Link} to={'/' + q.link_id}>{q.menu_name}</List.Header>
                                    <List.Content>{q.description}</List.Content>
                                </List.Content>
                            </List.Item>
                           
                        </>
                    )
                )}
            </List>
            {(showbottommenu===true) ? bottommenu : null}
        </Segment>
    </div>
     
    var help = 
        <Menu.Item fitted as={Link} to={headerlinks[2].link}>
           &nbsp; &nbsp;<Icon name='help' size='large'/>&nbsp;&nbsp;
        </Menu.Item>
    var settings = 
        <Menu.Item fitted as={Link} to={headerlinks[0].link}>
            &nbsp;&nbsp;<Icon name='setting' size='large'/>&nbsp;&nbsp;
        </Menu.Item>
    var notifications = null;

    /*
 <List.Description>
                                        {q.description}
                                    </List.Description>
    */

    return (
        <React.Fragment>
            <ConfirmationBox message='Are you sure you want to quit the application ?' open={open} onYes={handleConfirm} onNo={handleCancel}/>
            <Sidebar
                sidebar={sidebarcontent}
                open={sidebarOpen}
                docked={docked}
                shadow={sidebarshadow}
                styles={{ sidebar: { marginTop: "4.5em"  } }}
            >
                <Menu borderless size='small' fixed="top" pointing color={menubarcolor} inverted={menubarinverted}>
                    <Menu.Item onClick={setSidebar}>
                        <Icon name='sidebar'/>
                    </Menu.Item>
                    <Menu.Item as={Link} to={'/home'}>
                        <Image size="tiny" src="http://10.0.0.151/kpis_pabrik/logo.jpg" />
                    </Menu.Item>
                    <Menu.Item as={Link} to={'/home'}>
                        Home
                    </Menu.Item>
                    <Menu.Item>
                        {'Role : '}&nbsp;&nbsp;<b>{props.roleName}</b>
                    </Menu.Item>
                    <Menu.Menu position="right">
                        {help}
                        {settings}
                        {notifications}
                        <Menu.Item>
                            <Dropdown 
                                text={props.username} 
                                icon='user outline' 
                                className='icon'
                                floating
                                labeled
                                button
                                size='small'
                            >
                                <Dropdown.Menu>
                                    <Dropdown.Item onClick={profileClick}>Profile</Dropdown.Item>
                                    <Dropdown.Divider />
                                    <Dropdown.Item onClick={helpClick}>Help</Dropdown.Item>
                                    <Dropdown.Item onClick={signOutClick}>Sign Out</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </Menu.Item>
                    </Menu.Menu>
                </Menu>
                <Container style={{ marginTop: "5em"}}>{children}</Container>
            </Sidebar>
        </React.Fragment>
    );
}

const NavBar = props => {
    const [ visible, setVisible ] = useState(false)
    const { children } = props

    const handlePusher = () =>{
        if (visible) setVisible(false)
    };

    const handleToggle = () => {
        setVisible(!visible)
    }

    return (
        <MediaContextProvider>
            <Media lessThan="lg">
                <NavBarMobile
                    onPusherClick={handlePusher}
                    onToggle={handleToggle}
                    visible={visible}
                    sidebarmenus={props.sidebarmenus}
                    username={props.username}
                    showbottommenu={props.showbottommenu}
                    bottommenucaption={props.bottommenucaption}
                    menubarcolor={props.menubarcolor}
                    menubarinverted={props.menubarinverted}
                    sidebarcolor={props.sidebarcolor}
                    sidebarinverted={props.sidebarinverted}
                    sidebarshadow={props.sidebarshadow}
                    roleName={props.roleName}
                >  
                    {children}
                </NavBarMobile>
            </Media>
            <Media greaterThanOrEqual="lg">
                <NavBarDesktop
                    sidebarmenus={props.sidebarmenus}
                    showbottommenu={props.showbottommenu}
                    username={props.username}
                    bottommenucaption={props.bottommenucaption}
                    menubarcolor={props.menubarcolor}
                    menubarinverted={props.menubarinverted}
                    sidebarcolor={props.sidebarcolor}
                    sidebarinverted={props.sidebarinverted}
                    sidebarshadow={props.sidebarshadow}
                    roleName={props.roleName}
                >
                    {children}
                </NavBarDesktop>
            </Media>
        </MediaContextProvider>
      
    )
}

const AppBar = props => {
    const { children } = props
    //if (props.isLogin===true) {
        return (
            <div>
                <NavBar 
                    sidebarmenus={props.sidebarmenus} 
                    username={props.username}
                    showbottommenu={props.showbottommenu}
                    bottommenucaption={props.bottommenucaption}
                    menubarcolor={props.menubarcolor}
                    menubarinverted={props.menubarinverted}
                    sidebarcolor={props.sidebarcolor}
                    sidebarinverted={props.sidebarinverted}
                    sidebarshadow={props.sidebarshadow}
                    roleName={props.roleName}
                >
                    {children}
                </NavBar>
            </div>
        )
    //}
    /*
        else {
            return (
                <MediaContextProvider>
                    <Media greaterThanOrEqual="lg">
                        <Segment
                            inverted
                            textAlign='center'
                            style={{ padding: '1em 0em' }}
                            vertical
                        >
                            <Menu
                                fixed={fixed ? 'top' : null}
                                inverted={!fixed}
                                pointing={!fixed}
                                secondary={!fixed}
                                size='large'
                            >
                                <Container>
                                    <Menu.Item>
                                        <Image src='http://128.199.112.147:1338/dxl.jpg' size='mini' />
                                    </Menu.Item>
                                    <Menu.Item position='right'>
                                        <Button  as='a' inverted={!fixed}>Sign in</Button>   
                                    </Menu.Item>
                                </Container>
                            </Menu>
                        </Segment>
                        {children}
                    </Media>
                    <Media lessThan="lg">
                        <Segment
                            inverted
                            textAlign='center'
                            style={{ padding: '1em 0em' }}
                            vertical
                        >
                            <Menu
                                fixed={fixed ? 'top' : null}
                                inverted={!fixed}
                                pointing={!fixed}
                                secondary={!fixed}
                                size='mini'
                            >
                                <Container>
                                    <Menu.Item>
                                        <Image src='http://128.199.112.147:1338/dxl.jpg' size='mini' />
                                    </Menu.Item>
                                    <Menu.Item position='right'>
                                        <Button  as='a' inverted={!fixed}>Sign in</Button>   
                                    </Menu.Item>
                                </Container>
                            </Menu>
                        </Segment>
                        {children}
                    </Media>
                </MediaContextProvider>
            )
        }
    */
}

AppBar.propTypes = {
  children: PropTypes.node,
}

export default AppBar;
