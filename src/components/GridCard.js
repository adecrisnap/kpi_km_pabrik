import React, { useState, useEffect } from "react"
import EmptySpace from './EmptySpace';
import { 
    Card, 
    Image, 
    Container, 
    Grid, 
    Pagination,
    Button,
    Dropdown,
    Icon,
    Input,
    Menu
} from "semantic-ui-react"
import { useHistory, Link } from "react-router-dom";
import _ from 'lodash'

const SingleCard = ({ imageUrl, title, meta, description, onClick, footer }) => {
    return (
      <Card onClick={onClick} fluid>
        <Image src={imageUrl} wrapped ui={false} />
        <Card.Content>
            <Card.Header>{title}</Card.Header>
            <Card.Meta>
                <span className='date'>{meta}</span>
            </Card.Meta>
            <Card.Description>
                {description}
            </Card.Description>
        </Card.Content>
        <Card.Content extra>
            <a>
                {footer}
            </a>
        </Card.Content>
      </Card>
    )
}

const GridCard = props => {
    let history = useHistory();
    const { 
        datasource, 
        recordsperpage,
        columnsnumber,
        sortedby,
        ascdesc,
        link,
        showaddnew,
        showaddnewonempty,
        emptycaption,
        emptyimage,
        emptytitle,
        emptybuttoncaption,
        //optionssortby
    } = props

    const [ dataDisplay, setDataDisplay] = useState(null);
    const [ numberofpages, setNumberofpages ] = useState(1);
    const [ sortBy, setSortBy] = useState(sortedby)
    const [ ascDesc, setAscDesc ] = useState(ascdesc);
    const [ rowsFiltered, setRowsFiltered ] = useState(datasource);
    const [ activepage, setActivepage] = useState(1); 

    function GetSortOrder(prop) {    
        return function(a, b) {    
            if (a[prop] > b[prop]) {    
                return 1;    
            } else if (a[prop] < b[prop]) {    
                return -1;    
            }    
            return 0;    
        }    
    }    
  
    function GetSortOrderDesc(prop) {    
        return function(a, b) {    
            if (a[prop] > b[prop]) {    
                return -1;    
            } else if (a[prop] < b[prop]) {    
                return 1;    
            }    
            return 0;    
        }    
    }    

    function prepareData(datasource) {
        var idx = 0;
        var data = [];
        if (activepage>1) idx = (activepage-1) * recordsperpage;
        if (idx+recordsperpage<=datasource.length) {
          for(var i=idx;i<idx+recordsperpage;i++) {
              data.push(datasource[i]);
          }
        }
        else {
          for(var i=idx;i<datasource.length;i++) {
            data.push(datasource[i]);
          }
        }
        return data;
    }   

    useEffect(()=>{
        if (datasource!==null) {
            var ds1 = datasource;
            if (sortBy!==null && sortBy!==undefined) {
                if (ascDesc==='ascending' || ascDesc===null || ascDesc===undefined) 
                    ds1 = datasource.sort(GetSortOrder(sortBy));
                else
                    ds1 = datasource.sort(GetSortOrderDesc(sortBy));
            }
            var num = Math.ceil(ds1.length/recordsperpage);
            var data = prepareData(ds1)
            setNumberofpages(num);
            setRowsFiltered(datasource);
            setDataDisplay(data);
        }
    },[datasource])
  
    useEffect(()=>{
        if (rowsFiltered!==null) {
            var data = prepareData(rowsFiltered);
            setDataDisplay(data)
        }
    },[activepage])

    const onSearching = (e, { value }) => {
        setActivepage(1)
        if (datasource!==null) {
            var num = null
            var rowsf = null;
            var data = null;
            if (value!=='') {
                var val = value.toUpperCase();
                rowsf = datasource.filter(function(item) {
                    if (item.header!==undefined && item.header!==null)
                        if (item.header.toString().toUpperCase().includes(val)) {
                            return true;
                        }
                    if (item.subheader!==undefined && item.subheader!==null)
                        if (item.subheader.toString().toUpperCase().includes(val)) {
                            return true;
                        }
                    if (item.description!==undefined && item.description!==null)
                        if (item.description.toString().toUpperCase().includes(val)) {
                            return true;
                        }
                    if (item.footer!==undefined && item.footer!==null)
                        if (item.footer.toString().toUpperCase().includes(val)) {
                            return true;
                        }
                    return false;
                });
            }
            else {
                rowsf = datasource
            }
            num = Math.ceil(rowsf.length/recordsperpage);
            data = prepareData(rowsf);
            setNumberofpages(num);
            setRowsFiltered(rowsf)
            setDataDisplay(data);
        }
    }

    const onPageChange = (e, { activePage }) => {
        setActivepage(activePage);
    }

    const cardClick = (id,link,action) => {
        history.push(link + '/' + id)
    }

    const toTextValue = (value,customvalues) => {
        var found = false;
        var z = 0;
        var txt = '';
        while (found===false && z<customvalues.length) {
          if (customvalues[z].value===value) {
            found = true;
            txt = customvalues[z].text
          }
          z++;
        }
        if (found===true) 
          return txt
        else
          return '' 
    }

    const dropdownClick = (name,value) => {
        if (datasource!==null) {
            var ds1 = datasource;
            if (ascDesc==='ascending' || ascDesc===null || ascDesc===undefined) 
                ds1 = datasource.sort(GetSortOrder(value));
            else
                ds1 = datasource.sort(GetSortOrderDesc(value));
            var num = Math.ceil(ds1.length/recordsperpage);
            var data = prepareData(ds1);
            setSortBy(value)
            setNumberofpages(num);
            setRowsFiltered(datasource);
            setDataDisplay(data)
        }
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    return(
        <div>
            {(showaddnew!==null && showaddnew===true) ? 
                (
                    <Menu secondary>
                        <Menu.Item>
                            <Input icon='search' placeholder='Search...' />
                        </Menu.Item>
                        <Menu.Menu position='right'>
                            <Menu.Item>
                                {(datasource!==null) ? (activepage*recordsperpage-recordsperpage+1) + ' to ' + ((activepage*recordsperpage>datasource.length) ? datasource.length : activepage*recordsperpage) + ' of ' + (datasource.length) + ' entries' : ''}
                            </Menu.Item>
                            <Menu.Item>
                                <Pagination
                                    size='mini'
                                    floated='right'
                                    activePage={activepage}
                                    onPageChange={onPageChange}
                                    totalPages={numberofpages}
                                />
                            </Menu.Item>
                        </Menu.Menu>
                    </Menu>
                )
            :
                (
                    <Menu secondary>
                        <Menu.Item>
                            <Input icon='search' placeholder='Search...' />
                        </Menu.Item>
                    </Menu>
                )
            }
            {(datasource!==null && datasource.length>0) ? 
            (
            
                <Grid>
                    <Grid.Row>
                        <Grid.Column>
                            <Container className="container">
                                <Card.Group itemsPerRow={columnsnumber} stackable={true}>
                                    {dataDisplay &&
                                        dataDisplay.map(obj => {
                                        return (
                                            <SingleCard
                                                title={obj.header}
                                                meta={obj.subheader} 
                                                description={obj.description}
                                                onClick={() => {cardClick(obj.id,obj.link,obj.action)}}
                                                imageUrl={(obj.img) ? obj.img : null}
                                            />
                                        )
                                    })}
                                </Card.Group>
                            </Container>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            )
            : (
                
                <Grid>
                    <Grid.Row>
                        <Grid.Column>
                            <EmptySpace
                                title={(emptytitle!==null && emptytitle!==undefined) ? emptytitle : 'No result is found'}
                                caption={(emptycaption!==null && emptycaption!==undefined) ? emptycaption : 'No result is found'}
                                showButton={(showaddnewonempty!==null && showaddnewonempty!==undefined) ? showaddnewonempty : false}
                                image={(emptyimage!==null && emptyimage!==undefined) ? emptyimage : null}
                                buttonCaption={(emptybuttoncaption!==null && emptybuttoncaption!==undefined) ? emptybuttoncaption : 'Add new'}
                                link={link}
                            />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
               
            )}
        </div>
    )
}

export default GridCard;

/*
<Grid.Column floated='right' width='3'>
                            <Dropdown 
                                placeholder='Sort by'
                                fluid
                                selections
                                options={optionssortby}
                                onClick={dropdownClick}
                            />
                        </Grid.Column>

                          <Grid.Row columns={2}>
                        <Grid.Column floated='left'>
                            <Input icon='search' placeholder='Search...' onChange={onSearching}/>
                        </Grid.Column>
                        <Grid.Column floated='right'>
                            
                        </Grid.Column>
                    </Grid.Row>

*/