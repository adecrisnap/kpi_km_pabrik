import React, { useEffect, useState } from "react";
import {
  Container,
  Grid,
  Dropdown,
  Button,
  Segment,
  Header,
} from "semantic-ui-react";
import LoadingBox from "../components/LoadingBox";
import LoadingBox2 from "../components/LoadingBox";
import usePostData from "../services/usePostData";
import useGetData from "../services/useGetData";
import SimpleForm from "../components/SimpleForm";
import URLLocation from "../services/URLLocation";
import HeaderEnhanced from "../components/HeaderEnhanced";
import { useHistory } from "react-router-dom";
import Helper from "../services/Helper";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";
import "react-notifications/lib/notifications.css";
import Cookies from "js-cookie";

const KPIMapPlantEditor = (props) => {
    const role_id = Cookies.get('role_id')
  const rolegroup_id = Cookies.get('rolegroup_id')
  //const role_id = Cookies.get("role_id");
  let history = useHistory();
  const [openload, setOpenLoad] = useState(false);
  const [elements, setElements] = useState([]);
  const [action, setAction] = useState("get");

  const [levelKPI, setLevelKPI] = useState(null);

  const [plantOptions, setPlantOptions] = useState(null);
  const [plant, setPlant] = useState(null);

  const [KPIOptions, setKPIOptions] = useState(null);
  const [KPI, setKPI] = useState(null);

  const [RolesOptions, setRolesOptions] = useState(null);
  const [Roles, setRoles] = useState(null);

  const dropdownLevelKPIChange = (event, { value }) => {
    setLevelKPI(value);
  };

  const dropdownPlantChange = (event, { value }) => {
    setPlant(value);
  };

  const dropdownKPIChange = (event, { value }) => {
    setKPI(value);
  };

  const dropdownRolesChange = (event, { value }) => {
    setRoles(value);
  };

  const { statePost, setPostUrl, setPayload } = usePostData(null, null, "");

  const { stateGet, setGetUrl, setRefresh } = useGetData(null, "");

  useEffect(() => {
     if (levelKPI !== null) {
       // alert (levelKPI + role_id)
        if (levelKPI === 1)
        {
          setGetUrl(
            //setGetUrl(URLLocation.getUrl() + '/getkpisinlevel/' + level + '/' + role_id)
            URLLocation.getUrl() + '/getkpigroups1/1/' + role_id
          );
        } else {
        setGetUrl(
          //setGetUrl(URLLocation.getUrl() + '/getkpisinlevel/' + level + '/' + role_id)
          URLLocation.getUrl() + "/getkpisinlevel/" + levelKPI  + '/' + role_id
        );
      }
      setRefresh(true);
    } 
  }, [levelKPI])

   useEffect(() => {
    if (stateGet.status === 200 || stateGet.status === 201) {
      var data = [];
      for (var i = 0; i < stateGet.data.length; i++) {
        data.push({
          text: stateGet.data[i].group_name,
          value: stateGet.data[i].id,
        });
      }
      setKPIOptions(data);
    } else {
      if (stateGet.status !== 0)
        NotificationManager.error("Error", "Error", 3000);
    }
  }, [stateGet.randomstatus]);

  useEffect(() => {
    var options = [];
    var options2 = [];

    async function fetchAPIRoles() {
        const rolesApiUrl = URLLocation.getUrl() + '/getroleskpipabrik';     
        let response = await fetch(rolesApiUrl);
        response = await response.json();

        response.map((obj) => {
            options2.push({
              value: obj.id,
              text: obj.role_name,
            });
        });

        setRolesOptions(options2);
        setOpenLoad(false);
    }

    async function fetchAPIPlant() {
      const hoplant = Cookies.get("hoplant");
      if (hoplant === "2") {
        setOpenLoad(true);
        const apiUrl = URLLocation.getUrl() + "/getplantspabrik";
        let response = await fetch(apiUrl);
        response = await response.json();
        response.map((obj) => {
          options.push({
            value: obj.name_coy,
            text: obj.name_coy,
          });
        });
      } else {
        options.push({
          value: "HO",
          text: "HO",
        });
      }

      //setPlant("HO");
      setPlantOptions(options);
      setOpenLoad(false);
    }

    async function fetchPromiseAllPlantRoles() {

    }

    fetchAPIPlant();
    fetchAPIRoles();

    fetchPromiseAllPlantRoles();

  }, []);
  /* 
    useEffect(()=>{
        var optTemp = [];
        const apiUrl = URLLocation.getUrl() + '/getroles/1';
        setOpenLoad(true);
        fetch(apiUrl)
            .then(
                (response) => response.json()
                .then((data) => 
                    {
                        data.map((obj)=>{
                            optTemp.push({
                                value :obj._id + '|' +obj.rolename,
                                text : obj.rolename 
                            })
                        })
                        var val = [
                        
                            {   value : 0, options : optTemp },
                            {   value : 0 },
                            {   value : 0 },
                        ]
                        fillElements(val);
                        setOpenLoad(false)
                    }
                )
            )
            .catch((err)=>{
                setOpenLoad(false);
                NotificationManager.error(err, 'Error', 3000);
            })
    },[]) */

 /*  useEffect(() => {
    if (checkStatus() === true) {
      setSuccessMessage();
      history.goBack();
    } else {
      setErrorMessage();
    }
  }, [statePost.randomstatus]); */

  const levelOptions = [
    {
      text: "KPI Level 2",
      value: 2,
    },
    {
      text: "KPI Level 3",
      value: 3,
    },
  ];



  const setSuccessMessage = () => {
    if (action === "post")
      NotificationManager.success("Data saved", "Success", 3000);
  };

  const setErrorMessage = () => {
    var msg = "";
    if (action === "post") msg = statePost.errorMessage;
    if (msg !== "") NotificationManager.error(msg, "Error", 3000);
  };

  const checkStatus = () => {
    if (
      action === "post" &&
      (statePost.status === 200 || statePost.status === 201)
    )
      return true;
    else return false;
  };

  
  const setFilter = () => {
    if (KPI===null || Roles===null ) {
        NotificationManager.error('Parameter belum diisi lengkap','Error', 3000)
    }
    else {
        //setScreen(2)
        var data = {
            //plant : plant,
            kpi : KPI,
            role_id : parseInt(role_id),
            levelkpi : levelKPI,
            roles : Roles, 
        }
        //console.log(data);
        setPayload(data);
        setPostUrl(URLLocation.getUrl() + '/postkpiroleplant')
    }
  }
 
  useEffect(()=>{
    if (statePost.status===200 || statePost.status===201) {
            //alert("sukses")
            NotificationManager.success('Success','Success',3000)   
            history.goBack() 
    } else {
      //alert("gagal")
      var msg = '';
      msg = statePost.errorMessage;
      if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
    }
  },[statePost.randomstatus])
 
  return (
    <Container>
      <HeaderEnhanced
        title="Set Mapping Plant KPI Roles"
        text="Setting Map Plant KPI Roles"
      />
      <LoadingBox2 open={openload} />
      <NotificationContainer />
      {statePost.isLoading === true ? (
        <LoadingBox open={true} />
      ) : (
        <Grid stackable>
          <Grid.Row>
            <Grid.Column>
              <Grid columns={2}>
                {/* <Grid.Row>
                  <Grid.Column>
                    <label>Pilih Organisasi</label>
                    <Dropdown
                      placeholder="Pilih Organisasi"
                      selection
                      fluid
                      value={plant}
                      onChange={dropdownPlantChange}
                      options={plantOptions}
                      multiple={true}
                    />
                  </Grid.Column>
                </Grid.Row> */}
                <Grid.Row>
                  <Grid.Column>
                    <label>Pilih Level</label>
                    <Dropdown
                      placeholder="Pilih Level"
                      selection
                      fluid
                      value={levelKPI}
                      onChange={dropdownLevelKPIChange}
                      options={levelOptions}
                      multiple={false}
                    />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column>
                    <label>Pilih KPI</label>
                    <Dropdown
                      placeholder="Pilih KPI"
                      onChange={dropdownKPIChange}
                      options={KPIOptions}
                      selection
                      fluid
                      search
                      value={KPI}
                      multiple={false}
                    />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column>
                    <label>Pilih Roles Terkait</label>
                    <Dropdown
                      placeholder="Pilih Roles"
                      onChange={dropdownRolesChange}
                      options={RolesOptions}
                      selection
                      fluid
                      search
                      value={Roles}
                      multiple={true}
                    />
                  </Grid.Column>
                  <Grid.Column>
                      <Button positive onClick={setFilter}>Simpan Mapping</Button>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Grid.Column>
          </Grid.Row>
          
        </Grid>
      )}
    </Container>
  );
};

export default KPIMapPlantEditor;
