import React, { useEffect, useState } from 'react'
import { 
    Container,
    Grid,
    Dropdown,
    Button,
    Segment,
    Divider,
    Header
} from 'semantic-ui-react'
import LoadingBox from '../components/LoadingBox'
import useGetData from '../services/useGetData'
import usePostData from '../services/usePostData'
import URLLocation from '../services/URLLocation'
import HeaderEnhanced from '../components/HeaderEnhanced'
//import { useHistory } from "react-router-dom"
import {NotificationContainer, NotificationManager} from 'react-notifications'
import CSVReader from "react-csv-reader"
import useExportToExcel from '../services/useExportToExcel'
import 'react-notifications/lib/notifications.css'
import Cookies from 'js-cookie'


const TargetUpload = props => {
  
    const role_id = Cookies.get('role_id')
    const  { stateGet, setGetUrl, setRefresh } = useGetData(
        null,''
    )
    const { statePost, setPayload } = usePostData(URLLocation.getUrl() + '/uploadtargetscorepabrik',null,'')
    const [ tahun,setTahun] = useState(null)
    const [ plant, setPlant] = useState(null)
    const [ plantOptions, setPlantOptions] = useState(null)
    const { setApiData, setFileName, setExportToExcel } = useExportToExcel(null,null)
    const [ yearOptions, setYearOptions ] = useState(null)
  

    const dropdownChange = (event, {value} ) => {
        setTahun(value)
    }

    const dropdownChange2 = (event, {value} ) => {
        setPlant(value)
    }

    useEffect(()=>{
        var options = []
        const hoplant = Cookies.get('hoplant')
        if (hoplant==='2') {
            const apiUrl = URLLocation.getUrl() + '/getplantspabrik';
            fetch(apiUrl)
            .then((response) => response.json()
            .then((data) => {
                data.map((obj)=>{
                    options.push({
                        value :obj.name_coy,
                        text : obj.name_coy 
                    })
                })
            }))
        }
        else {
            options.push({
                value :'HO',
                text : 'HO' 
            })
        }
        setPlantOptions(options)
        var y1 = new Date().getFullYear()
        var y0 = y1 - 1;
        var y2 = y1 + 1
        const yopt = [
            {
                text: y0,
                value: y0,
            },
            {
                text: y1,
                value: y1,
            },
            {
                text: y2,
                value: y2,
            },
        ]
        setYearOptions(yopt)
        setTahun(new Date().getFullYear())
        //setBulan(new Date().getMonth() + 1)
    },[])

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201) {
            const data = stateGet.data
            setApiData(data)
            setFileName('C:\\target_kpi_' + tahun + '_' + plant + '_' + Cookies.get('rolename') + '.csv')
            setExportToExcel()
        }
        else {
            if (stateGet.status!==0) {
                var msg = '';
                msg = stateGet.errorMessage;
                if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
            }
        }
    },[stateGet.randomstatus])

   

    useEffect(()=>{
        if (statePost.status===200 || statePost.status===201) {
            NotificationManager.success('Data saved', 'Success', 3000)
        }
        else {
            if (statePost.status!==0) {
                var msg = '';
                msg = stateGet.errorMessage;
                if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
            }
        }
    },[statePost.randomstatus])

    const handleForce = (dataLoaded, fileInfo) => {
        console.log("masuk sini")
        console.log(dataLoaded, fileInfo);
        var arr = []
        for(var i=0;i<dataLoaded.length;i++) {
            arr.push({
                tahun : dataLoaded[i].tahun,
                bulan : dataLoaded[i].bln,
                level : dataLoaded[i].level,
                plant : dataLoaded[i].plant,
                id_kpi_group : dataLoaded[i].id_kpi_group,
                target_mtd : dataLoaded[i].target_mtd,
                target_ytd : dataLoaded[i].target_ytd,
                roleid : dataLoaded[i].role_id,
                uom : dataLoaded[i].uom
            })
        }
        var data = {
            data : arr
        }
        if (arr.length>0) {
            setPayload(data)
        }
        else
            NotificationManager.error('File tidak terbaca', 'Error', 3000);
     
    }

    const papaparseOptions = {
        header: true,
        dynamicTyping: true,
        skipEmptyLines: true,
        transformHeader: header => header.toLowerCase().replace(/\W/g, "_")
    }

    const generateExcel = () => {
        if (tahun!==null && plant!==null) {
            setGetUrl(URLLocation.getUrl() + '/gettargetbytahun/' + tahun + '/' + plant + '/' + role_id)
            setRefresh()
        }
        else
            NotificationManager.error('Pilih tahun dan organisasi', 'Error', 3000)
    }

    return (
        <Container>
            <HeaderEnhanced title='Target KPI' text='Untuk setting konfigurasi target KPI' />
            <NotificationContainer/>
            <Segment textAlign='left' style={{ marginLeft: "2.5em", marginRight: "2.5em", marginTop: "2em"}} raised>
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <>
                    <Grid textAlign='center' stackable>
                        <Grid.Row columns='1'>
                            <Grid.Column>
                                <Header as='h3' textAlign='left' style={{ marginLeft: '2.5em' }}>
                                    Upload Excel
                                </Header>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns='1'>
                            <Grid.Column>
                                <CSVReader
                                    cssClass="react-csv-input"
                                    label=""
                                    onFileLoaded={handleForce}
                                    parserOptions={papaparseOptions}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>     
                    
                    </>     
                )
            }
            </Segment>
        </Container>
    )
}

export default TargetUpload
