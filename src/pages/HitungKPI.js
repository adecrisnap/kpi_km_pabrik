import React, { useEffect, useState } from 'react'
import { 
    Container,
    Grid,
    Dropdown,
    Button,
    Segment,
    Header,
    Loader
} from 'semantic-ui-react'
import LoadingBox from '../components/LoadingBox'
import URLLocation from '../services/URLLocation'
import usePostData from '../services/usePostData';
import useGetData from '../services/useGetData';
import HeaderEnhanced from '../components/HeaderEnhanced'
import Helper from '../services/Helper'
import {NotificationContainer, NotificationManager} from 'react-notifications'
import useInterval from '../services/useInterval'
import 'react-notifications/lib/notifications.css'


const HitungKPI = props => {
    const { statePost, setPayload, setPostUrl } = usePostData(URLLocation.getUrl() + '/calculatekpi',null,'')
    const { stateGet, setGetUrl, setRefresh } = useGetData('','')
    const [ elements,setElements] = useState(null);
    const [ tahun,setTahun] = useState(null)
    const [ bulan, setBulan] = useState(null)
    const [ screen, setScreen] = useState(1)

    const [delay, setDelay] = useState(1000);
    const [isRunning, setIsRunning] = useState(false);
    const [totaltime, setTotaltime] = useState(30000)
    const [ yearOptions, setYearOptions ] = useState(null)

    useInterval(() => {
        //setRefresh(true)
        if (totaltime>0) {
            setTotaltime(totaltime-delay)
        }
        else {
            setIsRunning(false)
            setTotaltime(30000)
        }
    }, isRunning ? delay : null);

    /*
        const yearOptions = [
            {
            text: 2021,
            value: 2021,
            },
            {
            text: 2022,
            value: 2022,
            },
            {
            text: 2023,
            value: 2023,
            },
            {
            text: 2024,
            value: 2024,
            },
            {
            text: 2025,
            value: 2025,
            },
            {
            text: 2026,
            value: 2026,
            },
            {
                text: 2027,
                value: 2027,
            },
            {
                text: 2028,
                value: 2028,
            },
            {
                text: 2029,
                value: 2029,
            },
            {
                text: 2030,
                value: 2030,
            },
        ]    
    */
    const monthOptions = [
        {
            text : 'Januari',
            value : 1
        },
        {
            text : 'Februari',
            value : 2
        },
        {
            text : 'Maret',
            value : 3
        },
        {
            text : 'April',
            value : 4
        },
        {
            text : 'Mei',
            value : 5
        },
        {
            text : 'Juni',
            value : 6
        },
        {
            text : 'Juli',
            value : 7
        },
        {
            text : 'Agustus',
            value : 8
        },
        {
            text : 'September',
            value : 9
        },
        {
            text : 'Oktober',
            value : 10
        },
        {
            text : 'November',
            value : 11
        },
        {
            text : 'Desember',
            value : 12
        },
    ]

    const dropdownChange = (event, {value} ) => {
        setTahun(value)
    }

    const dropdownChange4 = (event, {value} ) => {
        setBulan(value)
    }

    const onClose = () => {
        //history.goBack();
        setScreen(1)
    } 

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    useEffect(()=>{
        if (statePost.status===200 || statePost.status===201) {
           //setGetUrl(URLLocation.getUrl() + '/getkpicalculationstatus')
           setIsRunning(true)
        }
        else {
            if (statePost.status!==0) NotificationManager.error('Error hitung capaian KPI','Error',3000)
        }
    },[statePost.randomstatus])

    useEffect(()=>{
        var y1 = new Date().getFullYear()
        var y0 = y1 - 1;
        var y2 = y1 + 1
        const yopt = [
            {
                text: y0,
                value: y0,
            },
            {
                text: y1,
                value: y1,
            },
            {
                text: y2,
                value: y2,
            },
        ]
        setYearOptions(yopt)
        setTahun(new Date().getFullYear())
        setBulan(new Date().getMonth())
    },[])

    /*
        useEffect(()=>{
            if (stateGet.status===200 || stateGet.status===201) {
            if (stateGet.data.message==='success')
                setIsRunning(false)
            }
            else {
                if (stateGet.status!==0) NotificationManager.error('Error hitung capaian KPI','Error',3000)
            }
        },[stateGet.randomstatus])
    */

    const setFilter = () => {
        if (tahun===null || bulan===null) {
            NotificationManager.error('Parameter belum diisi lengkap','Error', 3000)
        }
        else {
            setScreen(2)
            var data = {
                tahun : tahun,
                bulan : bulan,
            }
            setPayload(data)
        }
    }

    const setBack = () => {
        setScreen(1)
    }

    const fillElements = (val) =>{
    }
    
    //REGION FIXED OPERATIONS
    const operationToServer = (data,editUrl,addUrl) => {
    }

    const setSuccessMessage = () => {
    }

    const setErrorMessage = () => {
    }

    return (
        <Container>
            <HeaderEnhanced title='Kalkulasi Capaian KPI' text='Untuk menghitung capaian KPI secara otomatis' />
            <NotificationContainer/>
            {(screen===1) ? 
            (
                <Segment textAlign='left' style={{ marginLeft: "2.5em", marginRight: "2.5em", marginTop: "2em"}} raised>
                    <>
                        <Grid textAlign='center' stackable>
                            <Grid.Row>
                                <Grid.Column>
                                    <Header as='h3' textAlign='left' style={{ marginLeft: '2.5em' }}>
                                        Set Filter
                                        <Header.Subheader>Isi parameter tahun dan bulan untuk perhitungan capaian KPI</Header.Subheader>
                                    </Header>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column>
                                    <Grid columns={2}>
                                        <Grid.Row>
                                            <Grid.Column>
                                                <Dropdown
                                                    placeholder='Pilih Tahun'
                                                    selection
                                                    fluid
                                                    value={tahun}
                                                    onChange={dropdownChange}
                                                    options={yearOptions}
                                                />
                                            </Grid.Column>
                                        </Grid.Row>
                                        <Grid.Row>
                                            <Grid.Column>
                                                <Dropdown
                                                    placeholder='Pilih Bulan'
                                                    selection
                                                    fluid
                                                    value={bulan}
                                                    onChange={dropdownChange4}
                                                    options={monthOptions}
                                                />
                                            </Grid.Column>
                                        </Grid.Row>
                                        <Grid.Row>
                                            <Grid.Column>
                                                <Button positive onClick={setFilter}>Hitung Capaian</Button>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid> 
                        &nbsp;
                        <p/>    
                    </>     
                </Segment>
            )
            : 
            (
                (stateGet.isLoading===true || statePost.isLoading===true) ?
                    (
                        <LoadingBox open={true}/>
                    ) 
                    :
                    (
                        (isRunning===true) ? 
                        (
                            <Segment textAlign='left' style={{ marginLeft: "2.5em", marginRight: "2.5em", marginTop: "2em"}} raised>
                                <>
                                    <Grid textAlign='center' stackable>
                                        <Grid.Row>
                                            <Grid.Column>
                                                
                                                <Header as='h4' textAlign='left' style={{ marginLeft: '2.5em' }}>
                                                    Proses perhitungan capaian KPI masih berjalan...
                                                    <Loader active/>
                                                </Header>
                                                
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid> 
                                    &nbsp;
                                    <p/>    
                                </>     
                            </Segment>    
                        ) 
                        :
                        (
                            <Segment textAlign='left' style={{ marginLeft: "2.5em", marginRight: "2.5em", marginTop: "2em"}} raised>
                                <>
                                    <Grid textAlign='center' stackable>
                                        <Grid.Row>
                                            <Grid.Column>
                                                <Header as='h3' textAlign='left' style={{ marginLeft: '2.5em' }}>
                                                    Proses perhitungan capaian KPI sudah selesai...
                                                </Header>
                                            </Grid.Column>
                                        </Grid.Row>
                                        <Grid.Row>
                                            <Grid.Column>
                                                <Button primary onClick={setBack}>Kembali</Button>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid> 
                                    &nbsp;
                                    <p/>    
                                </>     
                            </Segment>    
                        )
                    )
            )}
        </Container>
    )
}

export default HitungKPI

