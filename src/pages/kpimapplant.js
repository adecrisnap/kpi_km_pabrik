import React, { useEffect, useState } from 'react';
import { 
    Container,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import useGetData from '../services/useGetData';
import GridTable from '../components/GridTable'
import URLLocation from '../services/URLLocation';
import HeaderEnhanced from '../components/HeaderEnhanced';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import Cookies from 'js-cookie';


const KPIMapPlant = props => {
    const role_id = Cookies.get('role_id')
    const rolegroup_id = Cookies.get('rolegroup_id')
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const  { stateGet,setGetUrl } = useGetData(
        null,''
    );

    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/getkpiroleplantall/');
    },[])

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];

            columns.push({columnname : 'id', columncaption : ' ', dataType : 'String', width : 5, link : 'kpimapplanteditor/edit', visible : false})
            columns.push({columnname : 'sequence', columncaption : '#', dataType: 'Number', width: 1});
            columns.push({columnname : 'group_name', columncaption : 'KPI Name', dataType: 'String', width: 8});
            columns.push({columnname : 'group_code', columncaption : 'Code', dataType: 'String', width: 5});
            columns.push({columnname : 'is_active', columncaption : 'Is Active', dataType: 'Custom', width: 2, customvalues : [ {value : 0, text : 'Non active'}, {value: 1, text :'Active'}]});
            //columns.push({columnname : 'lastmodified', columncaption : 'Last Modified', dataType: 'DateTime', width: 3});
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    return (
        <Container>
            <NotificationContainer/>
            <HeaderEnhanced title='KPI Plant Roles Mapping' text='melakukan mapping KPI Plant dengan Roles' />
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <GridTable
                        datasource={dataSource} 
                        columns={columns}
                        rowsperpage={10}
                        sortedby={'sequence'}
                        linkinrowlevel={true}
                        link={'kpimapplanteditor/add/0'}
                        ascdesc={'ascending'}
                        showaddnew={true}
                        showaddnewonempty={false}
                        emptycaption="Mapping belum terpasang"
                        emptyimage={null}
                        emptytitle="Tidak ada mapping"
                        emptybuttoncaption="Tambah Mapping"
                    />
                )
            }
        </Container>
    )
}

export default KPIMapPlant;
