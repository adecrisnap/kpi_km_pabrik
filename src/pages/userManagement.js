import React, { useEffect, useState } from 'react';
import { 
    Container,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import ConfirmationBox from '../components/ConfirmationBox';
import useGetData from '../services/useGetData';
import usePostData from '../services/usePostData'
import GridTable from '../components/GridTable'
import URLLocation from '../services/URLLocation';
import HeaderEnhanced from '../components/HeaderEnhanced';
import {NotificationContainer, NotificationManager} from 'react-notifications';



const UserManagement = props => {
    const [dataSource,setDataSource] = useState(null);
    const { statePost, setPayload} = usePostData(URLLocation.getUrl() + '/deleterolesroles')
    const [columns,setColumns] = useState(null);
    const [idtodelete,setIdtodelete] = useState(null)
    const [openconf, setOpenconf] = useState(false)
    const  { stateGet,setGetUrl, setRefresh } = useGetData(
        null,''
    );

    const {
        roleid,
    } = props.match.params

    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/getrolesroles/' + roleid);
    },[])


    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];
            columns.push({columnname : 'id', columncaption : '', dataType : 'Button',  width : 4, action : 'delete', buttonCaption : 'Delete', visible : true})
            columns.push({columnname : 'role_mg_name', columncaption : 'Nama', dataType: 'String', width: 12});
            columns.push({columnname : 'is_admin', columncaption : 'Admin',  dataType: 'Custom', width: 5, customvalues : [ {value : 1, text : 'Yes'}, {value: 0, text :'No'}]});
            columns.push({columnname : 'is_it', columncaption : 'IT Team',  dataType: 'Custom', width: 5, customvalues : [ {value : 1, text : 'Yes'}, {value: 0, text :'No'}]});
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])



    useEffect(()=>{
        if (statePost.status===200 || statePost.status===201) {
           setRefresh(true)
        }
        else {
            var msg = '';
            msg = statePost.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[statePost.randomstatus])

    const buttonClick = (value,action) =>{
        setIdtodelete(value);
        setOpenconf(true)
    }

    const handleCancel = () => {
        setOpenconf(false)
    }

    const handleConfirm = () => {
        setOpenconf(false)
        setPayload({
            id:idtodelete,
            
        })
    }

    return (
        <Container>
            <NotificationContainer/>
            <HeaderEnhanced title='Daftar Roles' text='Adalah semua roles yang terkait' />
            <ConfirmationBox message='Are you sure you want to delete ?' open={openconf} onYes={handleConfirm} onNo={handleCancel}/>
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <GridTable
                        datasource={dataSource} 
                        columns={columns}
                        rowsperpage={10}
                        internalclick={false}
                        sortedby={'role_mg_name'}
                        linkinrowlevel={false}
                        ascdesc={'ascending'}
                        showaddnew={true}
                        link={'/rolesrolesdetail/add/' +roleid}
                        buttonClick={buttonClick}
                        showaddnewonempty={false}
                        emptycaption="You don't have any related roles"
                        emptyimage={null}
                        emptytitle="No role"
                        emptybuttoncaption="Add new role"
                    />
                )
            }
        </Container>
    )
}

export default UserManagement;