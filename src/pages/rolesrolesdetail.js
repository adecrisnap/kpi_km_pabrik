import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import LoadingBox2 from '../components/LoadingBox';
import usePostData from '../services/usePostData';
import SimpleForm from '../components/SimpleForm'
import URLLocation from '../services/URLLocation';
import HeaderEnhanced from '../components/HeaderEnhanced';
import { useHistory } from "react-router-dom";
import Helper from '../services/Helper'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';


const RolesRolesDetail = props => {
    const { roleid } = props.match.params
    let history = useHistory();
    const [ openload, setOpenLoad] = useState(false)
    const [elements,setElements] = useState([]);
    const [action,setAction] = useState('get');
    const  { statePost, setPostUrl, setPayload } = usePostData(
        null, null,''
    );
   
    useEffect(()=>{
        var optTemp = [];
        const apiUrl = URLLocation.getUrl() + '/getroles/1';
        setOpenLoad(true);
        fetch(apiUrl)
            .then(
                (response) => response.json()
                .then((data) => 
                    {
                        data.map((obj)=>{
                            optTemp.push({
                                value :obj._id + '|' +obj.rolename,
                                text : obj.rolename 
                            })
                        })
                        var val = [
                        
                            {   value : 0, options : optTemp },
                            {   value : 0 },
                            {   value : 0 },
                        ]
                        fillElements(val);
                        setOpenLoad(false)
                    }
                )
            )
            .catch((err)=>{
                setOpenLoad(false);
                NotificationManager.error(err, 'Error', 3000);
            })
    },[])

    useEffect(()=>{
        if (checkStatus()===true)
        {
            setSuccessMessage();
            history.goBack()
        }
        else {
            setErrorMessage();
        }
    },[statePost.randomstatus])

    const fillElements = (val) =>{
        var elem = [];
      
        elem.push({
            type:'dropdown',
            name:'role_mg_id',
            label:'Role',
            multiple:false,
            value:val[0].value, 
            text:val[0].text,
            options:val[0].options
        })


        elem.push({
            type:'checkbox',
            name:'is_admin',
            label:'Is Admin',
            value:val[1].value, 
        })

        elem.push({
            type:'checkbox',
            name:'is_it',
            label:'Is IT',
            value:val[2].value, 
        })

      
        setElements(elem);
    }

    const onYes = () => {
        var val =  elements[0].value.split('|')
        var data = {
            role_id : roleid,
            role_mg_id : val[0],
            role_mg_name : elements[0].text,
            is_admin : elements[1].value,
            is_it : elements[2].value,
            is_active : 1,
        
        };
        operationToServer(data,null,'/setrolesroles')
    }

    //REGION FIXED OPERATIONS
    const operationToServer = (data,editUrl,addUrl) => {
        setPayload(data);
        setAction('post');
        setPostUrl(URLLocation.getUrl() + addUrl);
       
    }

    const setSuccessMessage = () => {
        if (action==='post') NotificationManager.success('Data saved', 'Success', 3000);
    }

    const setErrorMessage = () => {
        var msg = '';
        if (action==='post') msg = statePost.errorMessage;
        if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
    }

    const checkStatus = () => {
        if  (
            (action==='post' && (statePost.status===200 || statePost.status===201))
        )
            return true
        else
            return false
    }

    const onClose = () => {
        history.goBack();
    } 

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var val = value.split('|')
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        elems[idx].text = val[1];
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    return (
        <Container>
            <HeaderEnhanced title='Set Roles' text='Setting Roles' />
            <LoadingBox2 open={openload}/>
            <NotificationContainer/>
            {(statePost.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <SimpleForm
                                    elements={elements}
                                    yesCaption='Save'
                                    noCaption='Cancel'
                                    numberofcolumns={2}
                                    onClose={onClose}
                                    onChange={onChange}
                                    onDropdownChange={onDropdownChange}
                                    onCheckChange={onCheckChange}
                                    onYes={onYes}
                                    label={''}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>            
                )
            }
        </Container>
    )
}

export default RolesRolesDetail;
