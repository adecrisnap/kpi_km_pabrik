import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import LoadingBox2 from '../components/LoadingBox';
import useGetData from '../services/useGetData';
import usePostData from '../services/usePostData';
import usePutData from '../services/usePutData';
import SimpleForm from '../components/SimpleForm'
import URLLocation from '../services/URLLocation';
import HeaderEnhanced from '../components/HeaderEnhanced';
import { useHistory } from "react-router-dom";
import Helper from '../services/Helper'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import Cookies from 'js-cookie';


const KPILevel2Detail = props => {
    let history = useHistory();
   const role_id = Cookies.get('role_id')
   //const rolegroup_id = Cookies.get('rolegroup_id')
    const {
        id,
        mode
    } = props.match.params
    const is_it = Cookies.get('is_it')
    const [ openload, setOpenLoad] = useState(false)
    const [ idstate, setIDState ] = useState(id);
    const [ modestate, setModeState ] = useState(mode);
    const [elements,setElements] = useState([]);
    const [action,setAction] = useState('get');
    const  { stateGet, setGetUrl } = useGetData(
        null,''
    );
    const  { statePost, setPostUrl, setPayload } = usePostData(
        null, null,''
    );
    const  { statePut, setPutUrl, setPutload } = usePutData(
        null, null,''
    );
    const max_ach_opt = [
        {
            text : 'Maximum',
            value : 0
        },
        {
            text : 'Minimum',
            value : 1
        },
    ]
    const tipe_opt = [
        {
            text : 'Unlimited',
            value : 0
        },
        {
            text : 'Type 1',
            value : 1
        },
        {
            text : 'Type 2',
            value : 2
        },
        {
            text : 'Type 3',
            value : 3
        },
        {
            text : 'Type 4',
            value : 4
        },
    ]
    const ytd_opt = [
        {value : 0, text : 'Average'}, 
        {value: 1, text :'Accumulation'},
        {value: 2, text :'Last Value'},
        {value: 3, text :'Adjustment'}
    ]

    useEffect(()=>{
        if (modestate==='edit') {
            setAction('get');
            setGetUrl(URLLocation.getUrl() + '/getkpigroup2byid/' + idstate,'')
        }
        else {
            var optTemp = [];
            const apiUrl = URLLocation.getUrl() + '/getkpigroups1/1/' + role_id;
            setOpenLoad(true);
            fetch(apiUrl)
                .then((response) => response.json()
                .then((data) => 
                    {
                        data.map((obj)=>{
                            optTemp.push({
                                value :obj.id,
                                text : obj.group_name 
                            })
                        })
                        var val = [
                            {   value : ''  },
                            {   value : ''  },
                            {   value : ''  },
                            {   value : 1  },
                            {   value : 0, options : optTemp },
                            {   value : '' },
                           
                            {   value : 0, options : max_ach_opt},
                            {   value : 0, options : tipe_opt},
                            {   value : 0, options : ytd_opt},
                            {   value : 0 },
                            {   value : new Date() },
                            {   value : null, options : [] },
                            {   value : '' },
                            {   value : '' }
                        ]
                        fillElements(val);
                        setOpenLoad(false)
                        
                    }
                )
            )
            .catch((err)=>{
                setOpenLoad(false);
                NotificationManager.error(err, 'Error', 3000);
            })
        }
    },[])

    useEffect(()=>{
        if (checkStatus()===true)
        {
            var idtemp = id;   
            var group_code = null;
            var group_name = null;
            var sequence = null;
            var is_active = null;
            var id_kpi_group_1 = null;
            var sp_name = null;
            var lastmodified = null;
            var max_ach = null;
            var kpi_type = null;
            var ytd_measurement = null;
            var weight = null;

            //TODO:sini
            var childrenkpis = []
            var kpi_desc = ''
            var kpi_formula = ''

            
            if (action==='get') {
                group_code = stateGet.data[0].group_code;
                group_name = stateGet.data[0].group_name; 
                sequence = stateGet.data[0].sequence; 
                is_active = stateGet.data[0].is_active;
                id_kpi_group_1 = stateGet.data[0].id_kpi_group_1;
                sp_name = stateGet.data[0].sp_name;
                lastmodified = stateGet.data[0].lastmodified;
                max_ach = stateGet.data[0].max_ach;
                kpi_type = stateGet.data[0].kpi_type;
                ytd_measurement = stateGet.data[0].ytd_measurement;
                weight = stateGet.data[0].weight
                //alert(stateGet.data[0].childrenkpis)
                if (stateGet.data[0].childrenkpis!==null && stateGet.data[0].childrenkpis!=='') { 
                    var spt = stateGet.data[0].childrenkpis.split(',')
                    for(var i=0;i<spt.length;i++) {
                        childrenkpis.push(parseInt(spt[i]))
                    }
                }
                kpi_desc = stateGet.data[0].kpi_desc
                kpi_formula = stateGet.data[0].kpi_formula
            }
            if (action==='post') {
                setModeState('edit');
                idtemp = statePost.data.id;
            }
            if (action==='put' || action==='post') {
                group_code = elements[0].value
                group_name = elements[1].value
                sequence = elements[2].value
                is_active = elements[3].value
                id_kpi_group_1 = elements[4].value
                sp_name = elements[5].value
                lastmodified = elements[10].value
                max_ach = elements[6].value
                kpi_type = elements[7].value
                ytd_measurement = elements[8].value
                weight = elements[9].value
                childrenkpis = elements[11].value
                kpi_desc = elements[12].value
                kpi_formula = elements[13].value
            }
            var optTemp = [];
            var optTemp2 = []
            const apiUrl = URLLocation.getUrl() + '/getkpigroups1/1/' + role_id;
            setOpenLoad(true);
            fetch(apiUrl)
                .then((response) => response.json()
                .then((data) => 
                    {
                        data.map((obj)=>{
                            optTemp.push({
                                value :obj.id,
                                text : obj.group_name 
                            })
                        })
                       
                    }
                ).then(()=>{
                    fetch(URLLocation.getUrl() + '/getChildrenkpis/2/' + role_id)
                    .then((response) => {
                        return response.json()
                    })
                    .then((data)=>{
                        data.map((obj)=>{
                            optTemp2.push({
                                value :obj.id,
                                text : obj.group_name + ' - ' + obj.role_name
                            })
                        })
                        var val = [
                            {   value : group_code  },
                            {   value : group_name  },
                            {   value : sequence  },
                            {   value : is_active  },
                            {   value : id_kpi_group_1, options : optTemp  },
                            {   value : sp_name },
                        
                            {   value : max_ach, options : max_ach_opt},
                            {   value : kpi_type, options : tipe_opt},
                            {   value : ytd_measurement, options : ytd_opt},
                            {   value : weight },
                            {   value : lastmodified  },
                            {   value : childrenkpis, options : optTemp2},
                            {   value : kpi_desc },
                            {   value : kpi_formula}
                        ]
                        setOpenLoad(false)
                        setIDState(idtemp);
                        fillElements(val);
                        setSuccessMessage();
                    })
                })
            )
            .catch((err)=>{
                //alert(err)
                setOpenLoad(false);
                NotificationManager.error(err, 'Error', 3000);
            })
        }
        else {
            setErrorMessage();
        }
    },[stateGet.randomstatus,statePost.randomstatus,statePut.randomstatus])

    const fillElements = (val) =>{
        var elem = [];
        elem.push({
            type:'text',
            name:'group_code',
            label:'Code',
            placeholder:'Code',
            value:val[0].value, 
        })

        elem.push({
            type:'text',
            name:'group_name',
            label:'KPI Name',
            placeholder:'KPI Name',
            value:val[1].value, 
        })

        elem.push({
            type:'text',
            name:'sequence',
            label:'Sequence',
            placeholder:'Sequence',
            value:val[2].value, 
        })

        elem.push({
            type:'checkbox',
            name:'is_active',
            label:'Is Active',
            value:val[3].value, 
        })

        elem.push({
            type:'dropdown',
            name:'id_kpi_group_1',
            label:'KPI Level 1',
            multiple:false,
            value:val[4].value, 
            options:val[4].options
        })

        elem.push({
            type:'text',
            name:'sp_name',
            label:'Function',
            placeholder:'Function',
            value:val[5].value, 
            disabled : (is_it==='1') ? false : true
        })

       

        elem.push({
            type:'dropdown',
            name:'max_ach',
            label:'Max Ach',
            multiple:false,
            value:val[6].value, 
            options:val[6].options
        })

        elem.push({
            type:'dropdown',
            name:'kpi_type',
            label:'Tipe',
            multiple:false,
            value:val[7].value, 
            options:val[7].options
        })

        elem.push({
            type:'dropdown',
            name:'ytd_measurement',
            label:'YTD Measurement',
            multiple:false,
            value:val[8].value, 
            options:val[8].options
        })

        elem.push({
            type:'text',
            name:'weight',
            label:'Bobot (%)',
            placeholder:'Bobot (%)',
            value:val[9].value, 
        })


        elem.push({
            type:'label',
            datatype:'DATETIME',
            name:'lastmodified',
            label:'Last Modified',
            value:val[10].value, 
        })

        elem.push({
            type:'dropdown',
            name:'childKPIS',
            label:'Children KPIS',
            multiple:true,
            value:val[11].value, 
            options:val[11].options
        })

        elem.push({
            type:'memo',
            name:'kpi_desc',
            label:'Description',
            placeholder:'Description',
            value:val[12].value, 
           
        })

        elem.push({
            type:'memo',
            name:'kpi_formula',
            label:'Formula',
            placeholder:'Formula',
            value:val[13].value, 
           
        })
        setElements(elem);
    }

    const onYes = () => {
        var data = {
            group_code : elements[0].value,
            group_name : elements[1].value,
            sequence : elements[2].value,
            is_active : elements[3].value,
            id_kpi_group_1 : elements[4].value,
            sp_name : elements[5].value,
            max_ach : elements[6].value,
            kpi_type : elements[7].value,
            ytd_measurement : elements[8].value,
            weight : elements[9].value,
            roleid : role_id,
            childrenkpis : elements[11].value,
            kpi_desc : elements[12].value,
            kpi_formula : elements[13].value
        };
        operationToServer(data,'/editkpigroup2/' + idstate,'/createkpigroup2')
    }

    //REGION FIXED OPERATIONS
    const operationToServer = (data,editUrl,addUrl) => {
        if (modestate==='edit') {
            setPutload(data);
            setAction('put')
            setPutUrl(URLLocation.getUrl() + editUrl);
        }
        else {
            setPayload(data);
            setAction('post');
            setPostUrl(URLLocation.getUrl() + addUrl);
        }
    }

    const setSuccessMessage = () => {
        if (action==='put' || action==='post') NotificationManager.success('Data saved', 'Success', 3000);
    }

    const setErrorMessage = () => {
        var msg = '';
        if (action==='get') msg = stateGet.errorMessage;
        if (action==='put') msg = statePut.errorMessage;
        if (action==='post') msg = statePost.errorMessage;
        if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
    }

    const checkStatus = () => {
        if  ((action==='get' && (stateGet.status===200 || stateGet.status===201 || stateGet.status===304)) ||
            (action==='post' && (statePost.status===200 || statePost.status===201)) ||
            (action==='put' && (statePut.status===200 || statePut.status===201))
        )
            return true
        else
            return false
    }

    const onClose = () => {
        history.goBack();
    } 

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    const onDateChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].selectedDate = value;
        setElements(elems);
    }


    return (
        <Container>
            <HeaderEnhanced title='KPI Level 2' text='Pengelompokkan KPI Level 2' />
            <LoadingBox2 open={openload}/>
            <NotificationContainer/>
            {(stateGet.isLoading===true || statePost.isLoading===true || statePut.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <SimpleForm
                                    elements={elements}
                                    yesCaption='Save'
                                    noCaption='Cancel'
                                    numberofcolumns={2}
                                    onClose={onClose}
                                    onChange={onChange}
                                    onDropdownChange={onDropdownChange}
                                    onCheckChange={onCheckChange}
                                    onYes={onYes}
                                    label={''}
                                    
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>            
                )
            }
        </Container>
    )
}

export default KPILevel2Detail;
