import React, { useEffect, useState } from 'react'
import { 
    Container,
    Grid,
    Dropdown,
    Button,
    Segment,
    Divider,
    Header
} from 'semantic-ui-react'
import LoadingBox from '../components/LoadingBox'
import useGetData from '../services/useGetData'
import usePostData from '../services/usePostData'
import URLLocation from '../services/URLLocation'
import HeaderEnhanced from '../components/HeaderEnhanced'
//import { useHistory } from "react-router-dom"
import {NotificationContainer, NotificationManager} from 'react-notifications'
import CSVReader from "react-csv-reader"
import useExportToExcel from '../services/useExportToExcel'
import 'react-notifications/lib/notifications.css'
import Cookies from 'js-cookie'


const KonfigurasiDetail = props => {
    const role_id = Cookies.get('role_id')
    const  { stateGet, setGetUrl, setRefresh } = useGetData(
        null,''
    )
    const { statePost, setPayload } = usePostData(URLLocation.getUrl() + '/uploadconfiguration',null,'')
    const [ tahun,setTahun] = useState(null)
    const [ action, setAction ] = useState(0)
    const { setApiData, setFileName, setExportToExcel } = useExportToExcel(null,null)
    const [ yearOptions, setYearOptions ] = useState(null)
    
    /*
        const yearOptions = [
            {
            text: 2021,
            value: 2021,
            },
            {
            text: 2022,
            value: 2022,
            },
            {
            text: 2023,
            value: 2023,
            },
            {
            text: 2024,
            value: 2024,
            },
            {
            text: 2025,
            value: 2025,
            },
            {
            text: 2026,
            value: 2026,
            },
            {
                text: 2027,
                value: 2027,
            },
            {
                text: 2028,
                value: 2028,
            },
            {
                text: 2029,
                value: 2029,
            },
            {
                text: 2030,
                value: 2030,
            },
        ]    
    */

    const dropdownChange = (event, {value} ) => {
        setTahun(value)
    }

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201) {
            if (action===1) {
                const data = stateGet.data
                setApiData(data)
                setFileName('C:\\konfigurasi_kpi_pabrik_' + tahun + '.csv')
                setExportToExcel()
                setAction(0)
            }
            else {
                NotificationManager.success('Success', 'Success', 3000);
            }
        }
        else {
            if (stateGet.status!==0) {
                var msg = '';
                msg = stateGet.errorMessage;
                if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
            }
        }
    },[stateGet.randomstatus])

    useEffect(()=>{
        if (statePost.status===200 || statePost.status===201) {
            NotificationManager.success('Data saved', 'Success', 3000)
        }
        else {
            if (statePost.status!==0) {
                var msg = '';
                msg = stateGet.errorMessage;
                if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
            }
        }
    },[statePost.randomstatus])

    useEffect(()=>{
        var y1 = new Date().getFullYear()
        var y0 = y1 - 1;
        var y2 = y1 + 1
        const yopt = [
            {
                text: y0,
                value: y0,
            },
            {
                text: y1,
                value: y1,
            },
            {
                text: y2,
                value: y2,
            },
        ]
        setYearOptions(yopt)
        setTahun(new Date().getFullYear())
        //setBulan(new Date().getMonth() + 1)
    },[])

    const fillElements = (val) =>{
    }
    
    const onYes = () => {
    }

    //REGION FIXED OPERATIONS
    const operationToServer = (data,editUrl,addUrl) => {
    }

    const setSuccessMessage = () => {
    }

    const setErrorMessage = () => {
    }

    const handleForce = (dataLoaded, fileInfo) => {
    
        var err = false
        var bobotlvl2 = 0;
        var bobotlvl3 = 0;
        var arr = []
        for(var i=0;i<dataLoaded.length;i++) {
            if (dataLoaded[i].level===2) 
                bobotlvl2+=dataLoaded[i].weight
            else
                bobotlvl3+=dataLoaded[i].weight
            arr.push({
                tahun : dataLoaded[i].tahun,
                level : dataLoaded[i].level,
                id_kpi_group : dataLoaded[i].id_kpi_group,
                max_ach : dataLoaded[i].max_ach,
                weight : dataLoaded[i].weight,
                kpi_type : dataLoaded[i].kpi_type, 
                roleid : role_id
            })
        }
        if (bobotlvl2>0 && bobotlvl2!==100) err = true
        if (bobotlvl3>0 && bobotlvl3!==100) err = true
        var data = {
            data : arr
        }
        if (err===false) {
            if (arr.length>0)
                setPayload(data)
            else
                NotificationManager.error('File tidak terbaca', 'Error', 3000);
        }
        else
            NotificationManager.error('Bobot total tidak 100%', 'Error', 3000);
    }

    const papaparseOptions = {
        header: true,
        dynamicTyping: true,
        skipEmptyLines: true,
        transformHeader: header => header.toLowerCase().replace(/\W/g, "_")
    }

    const generateExcel = () => {
        if (tahun!==null) {
            setAction(1)
            setGetUrl(URLLocation.getUrl() + '/getconfigurationbytahun/' + tahun + '/' + role_id)
            setRefresh()
        }
        else {
            NotificationManager.error('Pilih tahun lebih dulu','Error', 3000)
        }
    }

    const copyConfig = () => {
        if (tahun!==null) {
            setAction(2)
            //setGetUrl(URLLocation.getUrl() + '/copyconfigurationbytahun/' + tahun + '/' + role_id)
            setGetUrl(URLLocation.getUrl() + '/copyconfigurationbytahunpabrik/' + tahun + '/' + role_id)
            setRefresh()
        }
        else {
            NotificationManager.error('Pilih tahun lebih dulu','Error', 3000)
        }
    }

    return (
        <Container>
            <HeaderEnhanced title='Konfigurasi Bobot dan Tipe' text='Untuk setting konfigurasi bobot dan tipe KPI' />
            <NotificationContainer/>
            <Segment textAlign='left' style={{ marginLeft: "2.5em", marginRight: "2.5em", marginTop: "2em"}} raised>
            {(stateGet.isLoading===true || statePost.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <>
                    <Grid textAlign='center' stackable>
                        <Grid.Row>
                            <Grid.Column>
                                <Header as='h3' textAlign='left' style={{ marginLeft: '2.5em' }}>
                                    Copy Konfigurasi
                                </Header>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <Grid>
                                    <Grid.Row>
                                        <Grid.Column>
                                            <Dropdown
                                                placeholder='Pilih Tahun'
                                                selection
                                                onChange={dropdownChange}
                                                options={yearOptions}
                                            />
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column>
                                        
                                      
                                            <Button primary onClick={copyConfig}>Copy Konfigurasi</Button>
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Grid.Column>
                           
                        </Grid.Row>
                    </Grid>     
                
                    </>     
                )
            }
            </Segment>
        </Container>
    )
}

export default KonfigurasiDetail

/* 
    <Divider vertical>Or</Divider>  
    <Button primary onClick={generateExcel}>Generate Excel</Button> 
     <Grid.Column>
                                <CSVReader
                                    //cssClass="react-csv-input"
                                    label=""
                                    onFileLoaded={handleForce}
                                    parserOptions={papaparseOptions}
                                />
                            </Grid.Column>
      <Grid.Column>
                                <Header as='h3' textAlign='left' style={{ marginLeft: '2.5em' }}>
                                    Upload Excel
                                </Header>
                            </Grid.Column>
    */