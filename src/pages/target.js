import React, { useEffect, useState } from 'react';
import { 
    Container,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import useGetData from '../services/useGetData';
import GridTable from '../components/GridTable'
import URLLocation from '../services/URLLocation';
import HeaderEnhanced from '../components/HeaderEnhanced';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import Cookies from 'js-cookie';


const Target = props => {
    const is_admin = Cookies.get('is_admin')
    const role_id = Cookies.get('role_id')
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const  { stateGet,setGetUrl } = useGetData(
        null,''
    );

    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/gettargetbyid/' + role_id);
    },[])

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];
            columns.push({columnname : 'id', columncaption : ' ', dataType : 'String', width : 5, visible : false})
            columns.push({columnname : 'tahun', columncaption : 'Tahun', dataType: 'String', width: 3});
            columns.push({columnname : 'bulan', columncaption : 'Bulan', dataType: 'Custom', width: 3, customvalues : [ {value : 1, text : 'Jan'},{value : 2, text : 'Feb'},{value : 3, text : 'Mar'},{value : 4, text : 'Apr'},{value : 5, text : 'May'},{value : 6, text : 'Jun'},{value : 7, text : 'Jul'},{value : 8, text : 'Aug'},{value : 9, text : 'Sep'},{value : 10, text : 'Oct'},{value : 11, text : 'Nov'},{value : 12, text : 'Dec'}]});
            columns.push({columnname : 'name_coy', columncaption : 'Org.', dataType: 'String', width: 3});
            columns.push({columnname : 'level', columncaption : 'Lvl', dataType: 'Number', width: 2});
            //columns.push({columnname : 'sequence', columncaption : '#', dataType: 'Number', width: 1});
            columns.push({columnname : 'group_name', columncaption : 'KPI', dataType: 'String', width: 9});

            columns.push({columnname : 'uom', columncaption : 'UOM', dataType: 'String',width: 3});
            columns.push({columnname : 'target_mtd', columncaption : 'Target (MTD)', dataType: 'Number',width: 5});
            columns.push({columnname : 'actual_mtd', columncaption : 'Actual (MTD)', dataType: 'Number',width: 5});
            columns.push({columnname : 'ach_mtd', columncaption : 'Ach %(MTD)', dataType: 'Number',width: 5});
            //columns.push({columnname : 'score_mtd', columncaption : 'Score (MTD)', dataType: 'Number',width: 5});

            columns.push({columnname : 'target_ytd', columncaption : 'Target (YTD)', dataType: 'Number',width: 5});
            columns.push({columnname : 'actual_ytd', columncaption : 'Actual (YTD)', dataType: 'Number',width: 5});
            columns.push({columnname : 'ach_ytd', columncaption : 'Ach %(YTD)', dataType: 'Number',width: 5});
            //columns.push({columnname : 'score_ytd', columncaption : 'Score (YTD)', dataType: 'Number',width: 5});
            //columns.push({columnname : 'lastmodified', columncaption : 'Last Modified', dataType: 'DateTime', width: 3});
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    return (
        <Container>
            <NotificationContainer/>
            <HeaderEnhanced title='Target KPI per Tahun/Bulan' text='Adalah target KPI per Tahun/Bulan' />
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <GridTable
                        datasource={dataSource} 
                        columns={columns}
                        rowsperpage={10}
                        sortedby={'bulan'}
                        linkinrowlevel={false}
                        //link={'targetdetail'}
                        link={'targetdetail2'}
                        ascdesc={'ascending'}
                        showaddnew={(is_admin==='1') ? true : false}
                        //addnewbuttoncaption='Upload/Generate target/score'
                        addnewbuttoncaption='Generate Excel Target'
                        showaddnewonempty={false}
                        emptycaption="Anda tidak memiliki target dan score KPI per tahun/bulan"
                        emptyimage={null}
                        emptytitle="Tidak ada target dan score per tahun/bulan"
                        emptybuttoncaption="Upload target"
                    />
                )
            }
        </Container>
    )
}

export default Target;
