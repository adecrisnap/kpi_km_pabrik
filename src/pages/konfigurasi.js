import React, { useEffect, useState } from 'react';
import { 
    Container,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import useGetData from '../services/useGetData';
import GridTable from '../components/GridTable'
import URLLocation from '../services/URLLocation';
import HeaderEnhanced from '../components/HeaderEnhanced';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import Cookies from 'js-cookie';


const Konfigurasi = props => {
    const role_id = Cookies.get('role_id')
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const  { stateGet,setGetUrl } = useGetData(
        null,''
    );

    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/getconfigurationbyid/' + role_id);
    },[])

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];

            columns.push({columnname : 'id', columncaption : ' ', dataType : 'String', width : 5, visible : false})
            columns.push({columnname : 'tahun', columncaption : 'Tahun', dataType: 'String', width: 2});
            columns.push({columnname : 'level', columncaption : 'Lvl', dataType: 'Number', width: 1});
            //columns.push({columnname : 'sequence', columncaption : '#', dataType: 'Number', width: 1});
            columns.push({columnname : 'group_name', columncaption : 'KPI', dataType: 'String', width: 10});
            columns.push({columnname : 'parent', columncaption : 'Parent', dataType: 'String', width: 10});
            columns.push({columnname : 'max_ach', columncaption : 'Max Ach.', dataType: 'Custom', width: 3, customvalues : [ {value : 0, text : 'Maximum'}, {value: 1, text :'Minimum'}]});
            columns.push({columnname : 'weight', columncaption : 'Bobot %', dataType: 'Number', width: 3});
            columns.push({columnname : 'kpi_type', columncaption : 'Tipe', dataType: 'Custom', width: 3, customvalues : [ {value : 0, text : 'Unlimited'}, {value: 1, text :'Type 1'}, {value: 2, text :'Type 2'},  {value: 3, text :'Type 3'},  {value: 4, text :'Type 4'}]});
            columns.push({columnname : 'ytd_measurement', columncaption : 'YTD Measurement', dataType: 'Custom', width: 3, customvalues : [ {value : 0, text : 'Average'}, {value: 1, text :'Accumulation'},{value: 2, text :'Last Value'},{value: 3, text :'Adjustment'} ]});
            //columns.push({columnname : 'lastmodified', columncaption : 'Last Modified', dataType: 'DateTime', width: 3});
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    return (
        <Container>
            <NotificationContainer/>
            <HeaderEnhanced title='Konfigurasi per Tahun' text='Adalah konfigurasi KPI per tahun' />
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <GridTable
                        datasource={dataSource} 
                        columns={columns}
                        rowsperpage={10}
                        sortedby={'tahun'}
                        linkinrowlevel={false}
                        link={'konfigurasidetail'}
                        ascdesc={'descending'}
                        showaddnew={true}
                        addnewbuttoncaption='Generate konfigurasi'
                        showaddnewonempty={false}
                        emptycaption="Anda tidak memiliki konfigurasi KPI per tahun"
                        emptyimage={null}
                        emptytitle="Tidak ada konfigurasi KPI per tahun"
                        emptybuttoncaption="Generate konfigurasi"
                    />
                )
            }
        </Container>
    )
}

export default Konfigurasi;
