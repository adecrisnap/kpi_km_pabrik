import React, {useEffect, useState} from 'react';
import PageContainer from './components/PageContainer';
import AppBar from './components/AppBar';
import { Redirect } from 'react-router';
import useAuthContext from './services/stores/useAuthContext'
import Cookies from 'js-cookie';


const HomeContainer = props => {
    const {
        storedData,
        fromLocal,
        idSelected
    } = props
    const { dispatch,stateContext } = useAuthContext()
    const [ sidebarmenus, setSidebarmenus ] = useState([])
    const [ username, setUsername] = useState('')
    const [ isEmpty, setIsEmpty] = useState(true)
    const [ roleName, setRoleName ] = useState('')
    const [ is_admin, setIs_admin] = useState(0)

    useEffect(()=>{
        var menus = []
        //var splt = idSelected.split('|')
        var splt = idSelected
        //alert(idSelected)
        //var is_admin = splt[2]
        //var is_it = splt[1]

        var is_admin = '1'
        var is_it = '1'

        setIs_admin(is_admin)

         Cookies.set('is_admin',is_admin)
         Cookies.set('is_it',is_it)
         Cookies.set('hoplant',2)
         Cookies.set('role_id', idSelected)
         Cookies.set('rolegroup_id', idSelected)
         //Cookies.set('rolename', 'Direktur Operasional');
         //setRoleName ('Direktur Operasional')

         switch(idSelected){
            case 223 :
                Cookies.set('rolename', 'Direktur Operasional (PABRIK)');
                setRoleName ('Direktur Operasional (PABRIK)');
                break;
            case 221 :
                Cookies.set('rolename', 'Manajer Pabrik (PABRIK)');
                setRoleName ('Manajer Pabrik (PABRIK)');
                break;
            case 220 :
                Cookies.set('rolename', 'Manajer Pembelian (PABRIK)');
                setRoleName ('Manajer Pembelian (PABRIK)');
                break;
            case 222 :
                Cookies.set('rolename', 'Manajer Kantor (PABRIK)');
                setRoleName ('Manajer Kantor (PABRIK)');     
                break;
         }  


        //Cookies.set('hoplant',splt[4])
        //Cookies.set('rolename',splt[3])
        if (is_admin==='1') {
            menus.push(
                {
                    icon_id : 'line graph', 
                    link_id : 'kpilevel1', 
                    menu_name : 'KPI Level 1',
                    id : 110,
                    description : 'Level pertama'
                },
                {
                    icon_id : 'line graph', 
                    link_id : 'kpilevel2', 
                    menu_name : 'KPI Level 2',
                    id : 111,
                    description : 'Level kedua'
                },
                {
                    icon_id : 'line graph', 
                    link_id : 'kpilevel3', 
                    menu_name : 'KPI Level 3',
                    id : 112,
                    description : 'Level ketiga'
                },
                {
                    icon_id : 'setting', 
                    link_id : 'konfigurasi', 
                    menu_name : 'Konf. Bobot & Tipe',
                    id : 113,
                    description : 'Atur bobot & tipe KPI'
                },
             
                {
                    icon_id : 'chart pie', 
                    link_id : 'hitungkpi', 
                    menu_name : 'Kalkulasi KPI',
                    id : 115,
                    description : 'Kalkulasi KPI'
                },
                
                {
                    icon_id : 'setting', 
                    link_id : 'mappingkpiplantroles', 
                    menu_name : 'Mapping KPI Plant Roles',
                    id : 118,
                    description : 'settingan mapping'
                },
               
            )
        }
        menus.push(
            {
                icon_id : 'target', 
                link_id : 'target', 
                menu_name : 'Target KPI',
                id : 114,
                description : 'Susun target KPI'
            },
             {
                icon_id : 'edit', 
                link_id : 'editscore', 
                menu_name : 'Edit Capaian KPI',
                id : 116,
                description : 'Edit manual capaian KPI'
            }, 
            {
                icon_id : 'upload', 
                link_id : 'uploadtarget', 
                menu_name : 'Upload Target KPI',
                id : 119,
                description : 'Upload target KPI'
            },
        )

        if (is_it==='1') {
            menus.push(
                {
                    icon_id : 'user', 
                    link_id : 'usermanagement', 
                    menu_name : 'Manajemen User',
                    id : 117,
                    description : 'Pengaturan user'
                },
            )
        }
        setUsername(storedData.partner_name)
        setIsEmpty(false)
        //setRoleName(splt[3])
        //setRoleName ('Direktur Operasional')
        setSidebarmenus(menus)
        if (fromLocal===true) {
            var payload = {
                id: storedData.id,
                partner_name :storedData.partner_name,
                menus : storedData.menus
            }
            dispatch({
                type: "LOGIN",
                payload: payload
            });
        }
    },[])

    /*
        useEffect(()=>{
            var data = {
                appcode : 2,
                menus : storedData.menus,
                menu_cat : idSelected
            }
            setPayload(data)
        }, []);

        useEffect(()=>{
            if (statePost.status===200 || statePost.status===201) {
                var menus = [];
                for (var i=0;i<statePost.data.length;i++) {
                    menus.push({
                        icon_id : statePost.data[i].Report_Icon, 
                        link_id : statePost.data[i].Report_Link, 
                        menu_name : statePost.data[i].Report_Name,
                        id : statePost.data[i].Report_ID,
                        description : statePost.data[i].Report_Desc
                    })
                }
                setUsername(storedData.partner_name)
                if (menus.length>0) {
                    setIsEmpty(false)
                    setSidebarmenus(menus)
                }
                else {
                    setIsEmpty(true)
                }
                if (fromLocal===true) {
                    var payload = {
                        id: storedData.id,
                        partner_name :storedData.partner_name,
                        menus : storedData.menus
                    }
                    dispatch({
                        type: "LOGIN",
                        payload: payload
                    });
                }
            }
        },[statePost.randomstatus])
    */

    useEffect(()=>{
        if (stateContext.isLogin===false) setIsEmpty(true)
    },[stateContext.isLogin])

    return (
        <React.Fragment>
                {(isEmpty===true) ? 
                (
                    <AppBar
                        roleName={roleName}
                        sidebarmenus={sidebarmenus}
                        username={username}
                        showbottommenu={false}
                        bottommenucaption=''
                        menubarcolor={'white'}
                        menubarinverted={false}
                        sidebarcolor={'black'}
                        sidebarinverted={true}
                        sidebarshadow={true}
                    >
                        <PageContainer/>
                        <Redirect to='/erroraccess' />
                    </AppBar>
                )
                : 
                (
                    <AppBar
                        roleName={roleName}
                        sidebarmenus={sidebarmenus}
                        username={username}
                        showbottommenu={false}
                        bottommenucaption=''
                        menubarcolor={'white'}
                        menubarinverted={false}
                        sidebarcolor={'black'}
                        sidebarinverted={true}
                        sidebarshadow={true}
                    >
                        <PageContainer/>
                        {(is_admin==='1') ? 
                            <Redirect to='/kpilevel1' />
                            :
                            <Redirect to='/editscore/' />
                        }
                    </AppBar>
                )
                
            }
           
        </React.Fragment>
    )
}

export default HomeContainer


/*

(stateContext.isLogin===true) ? 
                    (
                        <AppBar
                            sidebarmenus={sidebarmenus}
                            username={username}
                            showbottommenu={false}
                            bottommenucaption=''
                            menubarcolor={'white'}
                            menubarinverted={false}
                            sidebarcolor={'black'}
                            sidebarinverted={true}
                            sidebarshadow={true}
                        >
                            <PageContainer/>
                            <Redirect to='/kpilevel1' />
                        </AppBar>
                    )
                    :
                    (
                        <Login/>
                    )
            var menus = [
            
            {
                icon_id : 'line graph', 
                link_id : '/kpilevel1', 
                menu_name : 'KPI Level 1',
                description : 'Level pertama'
            },
            {
                icon_id : 'line graph',  
                link_id : '/kpilevel2', 
                menu_name : 'KPI Level 2',
                description : 'Level kedua'
            },
            {
                icon_id : 'line graph', 
                link_id : '/kpilevel3', 
                menu_name : 'KPI Level 3',
                description : 'Level ketiga'
            },
            {
                icon_id : 'setting', 
                link_id : '/konfigurasi', 
                menu_name : 'Konf. Bobot & Tipe',
                description : 'Atur bobot & tipe KPI'
            },
            {
                icon_id : 'target', 
                link_id : '/target', 
                menu_name : 'Target KPI',
                description : 'Susun target KPI'
            },
            {
                icon_id : 'chart pie', 
                link_id : '/hitungkpi/1', 
                menu_name : 'Kalkulasi KPI',
                description : 'Hitung capaian KPI'
            },

            {
                icon_id : 'edit', 
                link_id : '/editscore/1', 
                menu_name : 'Edit Capaian KPI',
                description : 'Edit manual capaian KPI'
            },
            
            /*
                {
                    icon_id : 'line graph', 
                    link_id : '/kpilevel1', 
                    menu_name : 'KPI Level 1',
                    description : ''
                },
                {
                    icon_id : 'line graph',  
                    link_id : '/kpilevel2', 
                    menu_name : 'KPI Level 2',
                    description : ''
                },
                {
                    icon_id : 'line graph', 
                    link_id : '/kpilevel3', 
                    menu_name : 'KPI Level 3',
                    description : ''
                },
                {
                    icon_id : 'setting', 
                    link_id : '/konfigurasi', 
                    menu_name : 'Konfigurasi Bobot dan Tipe',
                    description : ''
                },
                {
                    icon_id : 'target', 
                    link_id : '/target', 
                    menu_name : 'Target dan Capaian',
                    description : ''
                },
                {
                    icon_id : 'chart pie', 
                    link_id : '/target/2', 
                    menu_name : 'Kalkulasi KPI',
                    description : ''
                },

                {
                    icon_id : 'edit', 
                    link_id : '/editscore/1', 
                    menu_name : 'Edit Score KPI',
                    description : ''
                },
        
        ];
        */