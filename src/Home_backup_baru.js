import React, { useState, useEffect } from "react"
import { 
    Card, 
    Image, 
    Container, 
    Grid, 
    Header,
    Dropdown,
    Button
} from "semantic-ui-react"
import HomeContainer from "./HomeContainer"
import useAuthContext from './services/stores/useAuthContext'
import Login from './Login'
import useGetData from "./services/useGetData"
import URLLocation from "./services/URLLocation"
import usePostData from './services/usePostData'

const SingleCard = ({ imageUrl, title, meta, description, onClick, footer }) => {

    return (
      <Card onClick={onClick} fluid>
        <Image src={imageUrl} wrapped ui={false} />
        <Card.Content>
            <Card.Header>{title}</Card.Header>
            <Card.Meta>
                <span className='date'>{meta}</span>
            </Card.Meta>
            <Card.Description>
                {description}
            </Card.Description>
        </Card.Content>
        <Card.Content extra>
            <a>
                {footer}
            </a>
        </Card.Content>
      </Card>
    )
}

const Home = props => {
    const {
        storedData,
        fromLocal
    } = props
    const { stateContext } = useAuthContext()
    const [ dataHOPlant, setDataHOPlant] = useState(null)
    const [ dataOrgs, setDataOrgs ] = useState(null)
    const [ valHOPlant, setHOPlant ] = useState(null)
    const [ valOrg, setValOrg ] = useState(null)
    const [ idSelected, setIdSelected ] = useState(0)
    const { setGetUrl, stateGet, setRefresh } = useGetData(null)
    const { statePost, setPayload } = usePostData(URLLocation.getUrl() + '/getrolesbyselection',null)

    useEffect(()=>{
        const data = [
            {
                key : 1,
                text : "Head Office",
                value : 1
            },
            {
                key : 2,
                text : "Pabrik",
                value : 2
            }
        ]
        setDataHOPlant(data)
        setValOrg(null)
        setHOPlant(null)
    },[])

    useEffect(()=>{
        if (stateGet.status===200) {
            var orgs = []
            for(var i=0;i<stateGet.data.length;i++) {
                orgs.push({
                    key : stateGet.data[i].role_id,
                    value : stateGet.data[i].role_id + '|' + stateGet.data[i].is_it + '|' + stateGet.data[i].is_admin + '|' + stateGet.data[i].role_name + '|' + valHOPlant,
                    text : stateGet.data[i].role_name,
                })
            }
            setDataOrgs(orgs)
        }
    },[stateGet.randomstatus])

    const onClick = () => {
        if (valOrg!==null) setIdSelected(valOrg)
    }

    const dropdownChange1 = (event, {value} ) =>{
        setHOPlant(value)
        //const url = URLLocation.getUrl() + '/getrolesbyselection/' + value + '/' + stateContext.id
        //setGetUrl(url)
        //alert(JSON.stringify(stateContext.rolesfinal))
       
        setPayload({
            valhoplant : value,
            rolesfinal : stateContext.rolesfinal
        })
        setValOrg(null)
        setRefresh()
    }

    const dropdownChange2 = (event, {value} ) =>{
       
        setValOrg(value)
    }

    useEffect(()=>{
        if (statePost.status===200) {
            var orgs = []
            for(var i=0;i<statePost.data.length;i++) {
                orgs.push({
                    key : statePost.data[i].role_id,
                    value : statePost.data[i].role_id + '|' + statePost.data[i].is_it + '|' + statePost.data[i].is_admin + '|' + statePost.data[i].role_name + '|' + valHOPlant,
                    text : statePost.data[i].role_name,
                })
            }
            setDataOrgs(orgs)
        }
    },[statePost.randomstatus])


    useEffect(()=>{
        if (stateContext.isLogin===false) setIdSelected(0)
    },[stateContext.isLogin])

    return (
        <Container>
        {
            (stateContext.isLogin===true) ? (
                (idSelected===0 || idSelected===null || idSelected===undefined) ?
                (
                    <>
                        &nbsp;<p/>
                        <Header as='h1'>Pengelolaan KPI
                            <Header.Subheader>
                                Pilih KPI yang akan dikelola
                            </Header.Subheader>
                        </Header>
                        &nbsp;<p/>
                        <Grid>
                            <Grid.Row>
                                <Grid.Column width={5}/>
                                <Grid.Column width={6}>
                                    <Dropdown
                                        placeholder='Head Office/Pabrik'
                                        selection
                                        fluid
                                        value={valHOPlant}
                                        onChange={dropdownChange1}
                                        options={dataHOPlant}
                                    />
                                </Grid.Column>
                                 <Grid.Column width={5}/>
                            </Grid.Row>
                            <Grid.Row>
                            <Grid.Column width={5}/>
                                <Grid.Column width={6}>
                                    <Dropdown
                                        placeholder='Pilih organisasi'
                                        selection
                                        fluid
                                        value={valOrg}
                                        onChange={dropdownChange2}
                                        options={dataOrgs}
                                    />
                                </Grid.Column>
                                 <Grid.Column width={5}/>
                            </Grid.Row>
                            <Grid.Row>
                            <Grid.Column width={5}/>
                                <Grid.Column width={6}>
                                    <Button onClick={onClick} primary
                                    >
                                        Klik untuk lanjut
                                    </Button>
                                </Grid.Column>
                                 <Grid.Column width={5}/>
                            </Grid.Row>
                        </Grid>
                       
                    </>
                ) : 
                (
                    
                    <HomeContainer storedData={storedData} fromLocal={fromLocal} idSelected={idSelected}/>
                )
            )
            : 
            (
                <Login/>
            )            
        }
        </Container>
    )
}

export default Home