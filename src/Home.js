import React, { useState, useEffect } from "react"
import { 
    Card, 
    Image, 
    Container, 
    Grid, 
    Header,
    Dropdown,
    Button,
    Label
} from "semantic-ui-react"
import HomeContainer from "./HomeContainer"
import useAuthContext from './services/stores/useAuthContext'
import Login from './Login'
//import useGetData from "./services/useGetData"
import URLLocation from "./services/URLLocation"
import usePostData from './services/usePostData'


const SingleCard = ({ imageUrl, title, meta, description, onClick, footer }) => {

    return (
      <Card onClick={onClick} fluid>
        <Image src={imageUrl} wrapped ui={false} />
        <Card.Content>
            <Card.Header>{title}</Card.Header>
            <Card.Meta>
                <span className='date'>{meta}</span>
            </Card.Meta>
            <Card.Description>
                {description}
            </Card.Description>
        </Card.Content>
        <Card.Content extra>
            <a>
                {footer}
            </a>
        </Card.Content>
      </Card>
    )
}

const Home = props => {

    const {
        storedData,
        fromLocal
    } = props
    const { stateContext } = useAuthContext()

    const [ dataDisplay, setDataDisplay] = useState(null)
    const [ idSelected, setIdSelected ] = useState(0)

    // const [ dataHOPlant, setDataHOPlant] = useState(1)
    // const [ dataOrgs, setDataOrgs ] = useState(null)
    // const [ valHOPlant, setHOPlant ] = useState(1)
    // const [ valOrg, setValOrg ] = useState(null)
    // const [ idSelected, setIdSelected ] = useState(null)
    // //const { setGetUrl, stateGet, setRefresh } = useGetData(null)

    // const [ showDivDept, setShowDivDept ] = useState(true)
    // const [ divDept, setDivDept ] = useState([])
    // const [ selectDivDept, setSelectDivDept ] = useState(null)

    // const [ showDiv, setShowDiv ] = useState(false)
    // const [ div, setDiv ] = useState([])
    // const [ selectDiv, setSelectDiv ] = useState(null)

    // const [ showDept, setShowDept ] = useState(false)
    // const [ dept, setDept ] = useState([])
    // const [ selectDept, setSelectDept ] = useState(null)

    // const [ mode, setMode ] = useState(null)
    // const [ levelSelected, setLevelSelected ] = useState(null)
    
    // const { statePost, setPayload, setRefreshPost, setPostUrl } = usePostData(null,null)

    // useEffect(()=>{
    //     const data1 = [
    //         {
    //             key : 1,
    //             text : "Head Office",
    //             value : 1
    //         },
    //         {
    //             key : 2,
    //             text : "Pabrik",
    //             value : 2
    //         }
    //     ]
    //     setDataHOPlant(data1)
    //     const data2 = [
    //         {
    //             key : 1,
    //             text : "Division",
    //             value : 1
    //         },
    //         {
    //             key : 2,
    //             text : "Department",
    //             value : 2
    //         }
    //     ]
    //     setDivDept(data2)
    //     //setSelectDivDept(1)
    //     setShowDiv(false)
    //     setShowDept(false)
    //     setValOrg(null)
    //     setHOPlant(null)
    // },[])

    /*
        useEffect(()=>{
            if (stateGet.status===200) {
                var orgs = []
                for(var i=0;i<stateGet.data.length;i++) {
                    orgs.push({
                        key : stateGet.data[i].role_id,
                        value : stateGet.data[i].role_id + '|' + stateGet.data[i].is_it + '|' + stateGet.data[i].is_admin + '|' + stateGet.data[i].role_name + '|' + valHOPlant,
                        text : stateGet.data[i].role_name,
                    })
                }
                setDataOrgs(orgs)
            }
        },[stateGet.randomstatus])
    */

    // const onClick = () => {
    //     if (mode===1) {
    //         if (valOrg!==null) setIdSelected(valOrg)
    //     }
    //     else {
        
    //         if (selectDiv!==null && levelSelected===1) setIdSelected(selectDiv)
    //         if (selectDept!==null && levelSelected===2) setIdSelected(selectDept)
    //     }
    // }

    // const dropdownChange1 = (event, {value} ) =>{
    //     setHOPlant(value)
    //     if (value===1) {
    //         setLevelSelected(1)
    //         setSelectDivDept(1)
    //         setShowDivDept(true)
    //         setShowDiv(true)
    //         setShowDept(false)
    //         setMode(2)
        
    //         setPostUrl(URLLocation.getUrl() + '/getrolesbyselectiondivdept',null)
    //         var data = 
    //             {
    //                 valhoplant : 1,
    //                 rolesfinal : stateContext.rolesfinal,
    //                 isdiv : 1,
    //                 divid : null
    //             }
    //         setPayload(data)
    //         setRefreshPost()
    //     }
    //     else {
    //         setMode(1)
    //         setShowDivDept(false)
    //         setShowDiv(false)
    //         setShowDept(false)

    //         setPostUrl(URLLocation.getUrl() + '/getrolesbyselection',null)
    //         setPayload({
    //             valhoplant : 2,
    //             rolesfinal : stateContext.rolesfinal
    //         })
    //         setRefreshPost()

    //         setValOrg(null)

    //     }
    // }

    // const dropdownChange2 = (event, {value} ) =>{
    //     setValOrg(value)
    // }

    // const dropdownChange3 = (event, {value} ) =>{
    //     setMode(2)
    //     if (value===1) setLevelSelected(1)
    //     if (value===2) setLevelSelected(2)
    //     setSelectDivDept(value)
    //     setShowDiv(true)
    //     setSelectDept(null)
    //     setSelectDiv(null)
    //     setDiv([])
    //     setDept([])
    //     setPostUrl(URLLocation.getUrl() + '/getrolesbyselectiondivdept',null)
    //     var data = 
    //         {
    //             valhoplant : 1,
    //             rolesfinal : stateContext.rolesfinal,
    //             isdiv : 1,
    //             divid : null
    //         }
       
    //     setPayload(data)
    //     setRefreshPost()
    //     if (value===1) {

    //         setShowDept(false)
    //     }
    //     else {
    //         setShowDept(true)
    //     }
    // }

    // const dropdownChange4 = (event, {value} ) =>{
    //     setMode(3)
    //     setSelectDiv(value)
    //     setPostUrl(URLLocation.getUrl() + '/getrolesbyselectiondivdept',null)
    //     if (value!=='' && value!==null) {
    //         var divspt = value.split('|')
    //         var data = {
    //             valhoplant : 1,
    //             rolesfinal : stateContext.rolesfinal,
    //             isdiv : 2,
    //             divid : divspt[0]
    //         }
    //         setPayload(data)
    //         setSelectDept(null)
    //         setDept([])
    //         setRefreshPost()
    //     }
    // }

    // const dropdownChange5 = (event, {value} ) =>{
    //     setSelectDept(value)
    // }

    // useEffect(()=>{
    //     if (statePost.status===200) {
    //         var orgs = []
    //         for(var i=0;i<statePost.data.length;i++) {
    //             var txtadmin = (statePost.data[i].is_admin===1) ? ' - (Admin)' : ''
    //             orgs.push({
    //                 key : statePost.data[i].role_id,
    //                 value : statePost.data[i].role_id + '|' + statePost.data[i].is_it + '|' + statePost.data[i].is_admin + '|' + statePost.data[i].role_name + '|' + valHOPlant,
    //                 text : statePost.data[i].role_name  +  txtadmin,
    //             })
    //         }
    //         if (mode===1) {
    //             setDataOrgs(orgs)
    //         }
    //         if (mode===2) {
    //             setDiv(orgs)
    //         }
    //         if (mode===3) {
    //             setDept(orgs)
    //         }
    //     }
    // },[statePost.randomstatus])

    useEffect(()=>{
        const data = [{
            id : 223,
            header : 'KPI Direktur Operasional',
            subheader : 'Semua pengelolaan dan capaian KPI Direktur Operasional',
            footer : '',
            description : '',
            img : 'https://react.semantic-ui.com/images/avatar/large/matthew.png',
            link : 'kpilevel1',
            action :null
        },
        {
            id : 221,
            header : 'KPI Manajer Pabrik',
            subheader : 'Semua pengelolaan dan capaian KPI Manajer Pabrik',
            footer : '',
            description : '',
            img : 'https://react.semantic-ui.com/images/avatar/large/daniel.jpg',
            link : 'kpilevel1',
            action :null
        },
        {
            id : 220,
            header : 'KPI Manajer Pembelian',
            subheader : 'Semua pengelolaan dan capaian KPI Manajer Pembelian',
            footer : '',
            description : '',
            img : 'https://react.semantic-ui.com/images/avatar/large/steve.jpg',
            link : 'kpilevel1',
            action :null
        },
        {
            id : 222,
            header : 'KPI Manajer Kantor',
            subheader : 'Semua pengelolaan dan capaian Manajer Kantor',
            footer : '',
            description : '',
            img : 'https://react.semantic-ui.com/images/avatar/large/helen.jpg',
            link : 'kpilevel1',
            action :null
        }
        ]
        setDataDisplay(data)
    },[])



    const cardClick = (id,link,action) => {
        setIdSelected(id)
    }

    useEffect(()=>{
        if (stateContext.isLogin===false) setIdSelected(0)
    },[stateContext.isLogin])

    return (
        <Container>
        {
            (stateContext.isLogin===true) ? (
                (idSelected===0 || idSelected===null || idSelected===undefined) ?
                (
                    <>
                        &nbsp;<p/>
                        <Header as='h1'>Pengelolaan KPI
                        <Header.Subheader>
                        Pilih KPI yang akan dikelola
                        </Header.Subheader>
                        </Header>
                        &nbsp;<p/>
                        <Grid verticalAlign='middle'>
                            <Grid.Row>
                                <Grid.Column>
                                    <Card.Group itemsPerRow={4} stackable={true}>
                                        {dataDisplay &&
                                            dataDisplay.map(obj => {
                                            return (
                                                <SingleCard
                                                    title={obj.header}
                                                    meta={obj.subheader} 
                                                    description={obj.description}
                                                    onClick={() => {cardClick(obj.id,obj.link,obj.action)}}
                                                    imageUrl={(obj.img) ? obj.img : null}
                                                    footer={obj.footer}
                                                />
                                            )
                                        })}
                                    </Card.Group>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </>
                ) : 
                (
                    
                    <HomeContainer storedData={storedData} fromLocal={fromLocal} idSelected={idSelected}/>
                )
            )
            : 
            (
                <Login/>
            )            
        }
        </Container>
    )
}

export default Home


/*

 <Grid.Row>
                                <Grid.Column width={4}/>
                                <Grid.Column width={8}>
                                    <label>Lokasi</label>
                                    <Dropdown
                                        placeholder='Head Office/Pabrik'
                                        selection
                                        fluid
                                        value={valHOPlant}
                                        onChange={dropdownChange1}
                                        options={dataHOPlant}
                                    />
                                </Grid.Column>
                                 <Grid.Column width={4}/>
                            </Grid.Row>
*/