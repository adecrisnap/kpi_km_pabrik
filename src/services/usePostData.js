import { useState, useEffect, useReducer } from 'react'
import Helper from './Helper';

const dataFetchReducer = (statePost, action) => {
    switch (action.action.type) {
      case 'FETCH_INIT':
        return {
          ...statePost,
          isLoading: true,
          hasError: false
        };
      case 'FETCH_SUCCESS':
        return {
          ...statePost,
          isLoading: false,
          hasError: false,
          data: action.action.data,
          status : action.action.status,
          randomstatus : action.action.randomstatus
        };
      case 'FETCH_FAILURE':
        return {
          ...statePost,
          isLoading: false,
          hasError: true,
          errorMessage : action.action.errorMessage,
          status : action.action.status,
          randomstatus : action.action.randomstatus
        };
      default:
        throw new Error();
    }
};

const usePostData = (initialUrl, initialPayload, token) => {
    const [url, setPostUrl] = useState(initialUrl)
    const [refreshPost, setRefreshPostx ] = useState(null)
    const [payload, setPayload] = useState(initialPayload)
    const [statePost, dispatch] = useReducer(dataFetchReducer, {
        isLoading: false,
        hasError: false,
        data: null,
        errorMessage : '',
        status : 0,
        randomstatus : 0,
    });

    const setRefresh = () =>{
      setRefreshPostx(Helper.makeid())
    }

    const setRefreshPost = () =>{
      setRefreshPostx(Helper.makeid())
    }
    
    useEffect(() => {   
        const postData = () => {
            var action = 
            { 
              type : 'FETCH_INIT',
              errorMessage : '',
              status : 0,
              randomstatus : 0,
              data : null
            }
            dispatch({action});
            var pload = {}
            if (token!=='')
              pload = {
                method: "POST",
                  headers: {
                      'Authorization' : token,
                      'Content-Type' : 'application/json',
                  
                  },
                  
                  body: JSON.stringify(payload),
              }
            else 
              pload = {
                method: "POST",
                  headers: {
                      'Content-Type' : 'application/json',
                  },
                  
                  body: JSON.stringify(payload),
              }
            return fetch(url, pload)
            .then(r =>  r.json().then((data) => {
                if (r.status!==200 && r.status!==201 && r.status!==304) {
                    var action = 
                    { 
                      type : 'FETCH_FAILURE',
                      errorMessage : 'Error',
                      status : r.status,
                      randomstatus : Helper.makeid(),
                      data : null,
                     
                    }
                    dispatch({ action });
                }
                else {
                    var action = 
                    { 
                      type: 'FETCH_SUCCESS', 
                      data: data, 
                      status : r.status,
                      randomstatus : Helper.makeid(),
                      errorMessage : '',
                    }
                    dispatch({action});
                }
            }))
            .catch((error)=>{
                var action = 
                { 
                  type: 'FETCH_FAILURE', 
                  errorMessage : error.message, 
                  status : 500,
                  randomstatus : Helper.makeid(),
                  data : null
                }
                dispatch({action});
            })
        }
        if ((url!==null && payload!==null))
        postData()
    }, [payload, refreshPost])
    
    return { statePost, setPostUrl, setPayload, setRefresh, setRefreshPost }
}

export default usePostData