import Cookies from 'js-cookie'
export const reducer = (stateContext, action) => {
    switch (action.type) {
        case "LOGIN":
            var content = {
                    id: action.payload.id,
                    partner_name : action.payload.partner_name,
                    menus : action.payload.menus,
                    rolesfinal : action.payload.rolesfinal
            }
            localStorage.setItem('kpiData', JSON.stringify(content));
            return {
                ...stateContext,
                isLogin: true,
                id: action.payload.id,
                partner_name : action.payload.partner_name,
                menus : action.payload.menus,
                rolesfinal  : action.payload.rolesfinal
            };
        case "LOGOUT":
            localStorage.clear();
            Cookies.remove('is_admin')
            Cookies.remove('is_it')
            //Cookies.remove('role_id')
            Cookies.remove('rolegroup_id')
            Cookies.remove('hoplant')
            Cookies.remove('rolename')
            return {
                ...stateContext,
                isLogin: false,
                id : null,
                partner_name : null,
                menus : null,
                rolesfinal : null
                //tokens : null
            };
        default:
            return stateContext;
      }
}