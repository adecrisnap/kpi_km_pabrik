import React, { useState, useEffect } from "react"
import { 
    Card, 
    Image, 
    Container, 
    Grid, 
    Header
} from "semantic-ui-react"
import HomeContainer from "./HomeContainer"
import useAuthContext from './services/stores/useAuthContext'
import Login from './Login'
import HeaderEnhanced from './components/HeaderEnhanced';

const SingleCard = ({ imageUrl, title, meta, description, onClick, footer }) => {

    return (
      <Card onClick={onClick} fluid>
        <Image src={imageUrl} wrapped ui={false} />
        <Card.Content>
            <Card.Header>{title}</Card.Header>
            <Card.Meta>
                <span className='date'>{meta}</span>
            </Card.Meta>
            <Card.Description>
                {description}
            </Card.Description>
        </Card.Content>
        <Card.Content extra>
            <a>
                {footer}
            </a>
        </Card.Content>
      </Card>
    )
}

const Home = props => {
    const {
        storedData,
        fromLocal
    } = props
    const { stateContext } = useAuthContext()
    const [ dataDisplay, setDataDisplay] = useState(null)
    const [ idSelected, setIdSelected ] = useState(0)
    

    useEffect(()=>{
        const data = [{
            id : 1,
            header : 'KPI Direktur Operasional',
            subheader : 'Semua pengelolaan dan capaian KPI Direktur Operasional',
            footer : '',
            description : '',
            img : 'https://react.semantic-ui.com/images/avatar/large/matthew.png',
            link : 'kpilevel1',
            action :null
        },
        {
            id : 2,
            header : 'KPI Manajer Pabrik',
            subheader : 'Semua pengelolaan dan capaian KPI Manajer Pabrik',
            footer : '',
            description : '',
            img : 'https://react.semantic-ui.com/images/avatar/large/daniel.jpg',
            link : 'kpilevel1',
            action :null
        },
        {
            id : 3,
            header : 'KPI Manajer Pembelian',
            subheader : 'Semua pengelolaan dan capaian KPI Manajer Pembelian',
            footer : '',
            description : '',
            img : 'https://react.semantic-ui.com/images/avatar/large/steve.jpg',
            link : 'kpilevel1',
            action :null
        },
        {
            id : 4,
            header : 'KPI Manajer Kantor',
            subheader : 'Semua pengelolaan dan capaian Manajer Kantor',
            footer : '',
            description : '',
            img : 'https://react.semantic-ui.com/images/avatar/large/helen.jpg',
            link : 'kpilevel1',
            action :null
        }
        ]
        setDataDisplay(data)
    },[])

    const cardClick = (id,link,action) => {
        setIdSelected(id)
    }

    useEffect(()=>{
        if (stateContext.isLogin===false) setIdSelected(0)
    },[stateContext.isLogin])

    return (
        <Container>
        {
            (stateContext.isLogin===true) ? (
                (idSelected===0 || idSelected===null || idSelected===undefined) ?
                (
                    <>
                        &nbsp;<p/>
                        <Header as='h1'>Pengelolaan KPI
                        <Header.Subheader>
                        Pilih KPI yang akan dikelola
                        </Header.Subheader>
                        </Header>
                        &nbsp;<p/>
                        <Grid verticalAlign='middle'>
                            <Grid.Row>
                                <Grid.Column>
                                    <Card.Group itemsPerRow={4} stackable={true}>
                                        {dataDisplay &&
                                            dataDisplay.map(obj => {
                                            return (
                                                <SingleCard
                                                    title={obj.header}
                                                    meta={obj.subheader} 
                                                    description={obj.description}
                                                    onClick={() => {cardClick(obj.id,obj.link,obj.action)}}
                                                    imageUrl={(obj.img) ? obj.img : null}
                                                    footer={obj.footer}
                                                />
                                            )
                                        })}
                                    </Card.Group>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </>
                ) : 
                (
                    
                    <HomeContainer storedData={storedData} fromLocal={fromLocal} idSelected={idSelected}/>
                )
            )
            : 
            (
                <Login/>
            )            
        }
        </Container>
    )
}

export default Home