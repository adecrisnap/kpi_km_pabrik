import React, { useReducer, useEffect, useState } from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css'
import { AuthContext } from './services/stores/AuthContext';
import { initialState } from './services/stores/initialState';
import { reducer } from './services/stores/reducer';
import { BrowserRouter } from 'react-router-dom';
import Home from './Home';
import Login from './Login'


function App() {
  const [stateContext, dispatch] = useReducer(reducer, initialState);
  //const [ storedData ] = useState(JSON.parse(localStorage.getItem('kpiData')));
  let [ storedData ] = useState(null);
  return (
      <BrowserRouter>
          <AuthContext.Provider
            value={{
              stateContext,
              dispatch
            }}
          >
             {(()=> {
                  storedData = JSON.parse(localStorage.getItem('kpiData'))
                  if (storedData!==null && storedData!==undefined) {
                    return  <div className="App">
                              <Home storedData={storedData} fromLocal={true}/>
                            </div>
                  }
                  else {
                    return  <div className="App">
                              <Login/>
                            </div>
                  }  
                })()}
          </AuthContext.Provider>
      </BrowserRouter>
    );
}

export default App;
